import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { AppState } from 'app/states/app/state';


@Injectable()
export class UserHomeGuard implements CanActivate {

    constructor (private store: Store, private router: Router) {};


    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {

        const user: any = this.store.selectSnapshot(AppState.user);
        if (user && user['_id']) {
            return true;
        }

        this.router.navigate(['/auth/login']);
        return false;
        

    }
}
