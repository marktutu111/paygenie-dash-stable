import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { AppState } from 'app/states/app/state';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EmployerChangePinGuard implements CanActivate {
    constructor(private store:Store,private router:Router,private route:ActivatedRoute){};
        canActivate(
            route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot
        ): Observable<boolean> | Promise<boolean> | boolean {
            const user=this.store.selectSnapshot(AppState.user);
            if(user.active){
                return true;
            }

            this.router.navigate(['employer/cpass']);
            return false;
    
        }

    
}
