import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { UserState } from 'app/states/users/users.state';


@Injectable()
export class ResetPinGuard implements CanActivate {

    constructor (private router: Router, private store: Store) {};

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        try {
            const temp = this.store.selectSnapshot(UserState.temp);
            if (!temp && !temp.account && !temp.empId) {
                throw Error(null);
            }
            return true;
        } catch (err) {
            this.router.navigate(['./auth/login']);
            return false;
        }
        
    }


}
