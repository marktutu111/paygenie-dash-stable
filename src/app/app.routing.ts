import { Routes } from "@angular/router";
import { EmployerHomeGuard } from "./guards/employer.guard";
import { UserHomeGuard } from "./guards/user-home.guard";
import { AdminLoginGuard } from "./guards/admin-login-guard";

export const AppRoutes: Routes = [
  {
    path: "",
    redirectTo: "customer",
    pathMatch: "full",
  },
  {
    path: "employer-login",
    loadChildren: () =>
      import("./payworld/auth/employer-login/employer-login.module").then(
        (m) => m.EmployerLoginModule
      ),
  },
  {
    path: "admin-login",
    loadChildren: () =>
      import("./payworld/auth/admin-login/admin-login.module").then(
        (m) => m.AdminLoginModule
      ),
  },
  {
    path: "customer",
    loadChildren: () =>
      import(
        "./payworld/auth/terms-and-conditions/terms-and-conditions.module"
      ).then((m) => m.TermsandConditionsModule),
  },
  {
    path: "employer",
    canActivate: [EmployerHomeGuard],
    loadChildren: () =>
      import("./payworld/employer/employer.module").then(
        (m) => m.EmployerModule
      ),
  },
  {
    path: "user",
    canActivate: [UserHomeGuard],
    loadChildren: () =>
      import("./payworld/users/users.module").then((m) => m.UsersModule),
  },
  {
    canActivate: [AdminLoginGuard],
    path: "admin",
    loadChildren: () =>
      import("./payworld/admin/admin.module").then((m) => m.AdminModule),
  },
  {
    path: "**",
    redirectTo: "customer",
  },
];
