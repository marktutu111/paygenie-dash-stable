

export class Toggle {
    static readonly type = '[ProgressBar] Toggle]';
    constructor(public readonly toggle: boolean = false) {};
}
