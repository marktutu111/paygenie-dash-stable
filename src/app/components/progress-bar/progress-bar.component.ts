import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { ProgressBarState } from './progress-bar.state';

@Component({
    selector: 'app-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {

    @Input()
    toggle: boolean = false;

    constructor(private store: Store) {};

    ngOnInit(): void {
        
        this.store.select(ProgressBarState)
                  .subscribe(({ loading }) => this.toggle = loading);
    };


}
