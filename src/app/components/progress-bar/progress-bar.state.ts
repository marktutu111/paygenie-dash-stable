import { State, Select, Action, StateContext } from '@ngxs/store';
import { Toggle } from './progress-bar.actions';

interface StateModel {
    loading: boolean;
}

@State<StateModel>({
    name: 'ProgressBar',
    defaults: {
        loading: false
    }
})
export class ProgressBarState {

    @Select()
    static loading (state: StateModel) {
        return state.loading;
    }

    @Action(Toggle)
    toggle ({ patchState }: StateContext<StateModel>, { toggle }: Toggle) {
        window.scrollTo(0,0);
        patchState(
            {
                loading: toggle
            }
        )
    }


}
