import { environment } from "environments/environment";
let URL: string = '';
if (environment.production) {
    URL = 'https://app.mypaygenie.com/paygenie';
} else {
    URL = 'http://localhost:3025/api';

}
export const BASE_URL = URL;