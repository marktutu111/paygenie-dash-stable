export const admin = [
  {
    path: "dashboard",
    title: "Dashboard",
    type: "link",
    icontype: "dashboard",
  },
];

export const employerMenu = [
  {
    path: "dashboard",
    title: "Dashboard",
    type: "link",
    icontype: "dashboard",
  },
  {
    path: "loans",
    title: "Customer Loans",
    type: "link",
    icontype: "money",
  },
  {
    path: "list",
    title: "Customers",
    type: "link",
    icontype: "person",
  },
  {
    path: "new",
    title: "Add Customer",
    type: "link",
    icontype: "person_add",
  },
  {
    path: "upload",
    title: "Upload Customers",
    type: "link",
    icontype: "attach_file",
  },
  {
    path: "invoice",
    title: "Payment Invoice",
    type: "link",
    icontype: "attach_file",
  },
  {
    path:'cpass',
    title: "Change Password",
    type: "link",
    icontype: "lock",
  }
  // {
  //     path: 'reports',
  //     title: 'Reports',
  //     type: 'link',
  //     icontype: 'report'
  // },
];

export const adminMenu = [
  // {
  //     path: 'dashboard',
  //     title: 'Dashboard',
  //     type: 'link',
  //     icontype: 'dashboard'
  // },
  {
    path: "loans",
    title: "Customer Loans",
    type: "link",
    icontype: "money",
  },
  {
    path: "employers",
    title: "Employers",
    type: "link",
    icontype: "money",
  },
  {
    path: "addemployer",
    title: "Add Employer",
    type: "link",
    icontype: "money",
  },
  {
    path: "customers",
    title: "Customers",
    type: "link",
    icontype: "person",
  },
  {
    path: "addcustomer",
    title: "Add Customer",
    type: "link",
    icontype: "person_add",
  },
  {
    path: "uploadcustomers",
    title: "Upload Customers",
    type: "link",
    icontype: "person_add",
  },
  {
    path: "invoices",
    title: "Invoices",
    type: "link",
    icontype: "money",
  },
  {
    path: "payinvoice",
    title: "Pay Invoice",
    type: "link",
    icontype: "attach_file",
  },
  {
    path: "settings",
    title: "Loan Settings",
    type: "link",
    icontype: "attach_file",
  },
  // {
  //     path: 'reports',
  //     title: 'Reports',
  //     type: 'link',
  //     icontype: 'report'
  // },
];

export const usersMenu = [
  {
    path: "profile",
    title: "Profile",
    type: "link",
    icontype: "person",
  },
  {
    path: "loans",
    title: "Your Loans",
    type: "link",
    icontype: "history",
  },
  {
    path: "loanrequest",
    title: "Loan Request",
    type: "link",
    icontype: "money",
  },
  {
    path: "billpay",
    title: "Bill Pay",
    type: "link",
    icontype: "text_snippet",
  },
];
