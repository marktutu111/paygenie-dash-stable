


export class ToggleSideContent {
    static readonly type = '[App] ToggleSideContent';
}


export class ChangePath {
    static readonly type = '[App] ChangePath';
    constructor(public readonly path: string) {};
}


export class AccountType {
    static readonly type = '[App] AccountType';
    constructor(public readonly account: 'admin' | 'employer' | 'user') {};
}


export class SetUser {
    static readonly type = '[App] SetUser]';
    constructor(public readonly user: any) {};
}


export class Logout {
    static readonly type = '[App] Logout]';
}


export class AutoLogin {
    static readonly type = '[App] AutoLogin';
}


export class GetSummary {
    static readonly type = '[App] GetSummary]';
}


export class GetAdminSummary {
    static readonly type = '[App] GetAdminSummary]';
}