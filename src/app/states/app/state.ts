import { State, Selector, Action, StateContext } from '@ngxs/store';
import { ToggleSideContent, AccountType, SetUser, Logout, AutoLogin, GetSummary, GetAdminSummary } from './actions';
import { Navigate } from "@ngxs/router-plugin";
import { CustomersModel } from 'app/models/customers/customers.model';
import { AggregateService } from 'app/services/aggregate/aggregate.service';





export interface AppStateModel {
    toggled: boolean;
    account_type: 'user' | 'employer' | 'admin';
    user: any;
    summary:any;
}

@State<AppStateModel>({
    name: 'App',
    defaults: {
        toggled: false,
        account_type: 'user',
        user: null,
        summary:null
    }
})
export class AppState {

    @Selector()
    static toggled (state: AppStateModel) {
        return state.toggled;
    }

    @Selector()
    static summary (state: AppStateModel) {
        return state.summary;
    }


    @Selector()
    static user (state: AppStateModel) {
        return state.user;
    }


    @Selector()
    static account_type (state: AppStateModel) {
        return state.account_type;
    }

    constructor(private service:AggregateService){}


    @Action(ToggleSideContent)
    toggleSideContent ({ patchState, getState }: StateContext<AppStateModel>) {
        return patchState({
            toggled: !getState().toggled
        })
    }
    

    @Action(GetSummary)
    async getSummary({patchState}:StateContext<AppStateModel>){
        try {
            const response=await this.service.getSummary();
            if (!response.success){
                throw Error(response.message);
            }
            return patchState(
                {
                    summary:response.data
                }
            )
        } catch (err) {

        }
    }

    @Action(GetAdminSummary)
    async getAdminSummary({patchState}:StateContext<AppStateModel>){
        try {
            const response=await this.service.getAdminSummary();
            if (!response.success){
                throw Error(response.message);
            }
            return patchState(
                {
                    summary:response.data
                }
            )
        } catch (err) {

        }
    }
    

    @Action(AccountType)
    accountType ({ patchState }: StateContext<AppStateModel>, { account }: AccountType) {
        return patchState(
            {
                account_type: account
            }
        )
    }


    
    @Action(SetUser)
    setUser ({ patchState }: StateContext<AppStateModel>, { user }: SetUser) {
        sessionStorage.setItem('paygenie-user', JSON.stringify(user))
        return patchState(
            {
                user: user
            }
        )
    }



    @Action(Logout)
    logout ({ patchState, dispatch,getState }: StateContext<AppStateModel>) {
        const account=getState().account_type;
        patchState(
            {
                user: null
            }
        )
        switch (account) {
            case 'admin':
                return dispatch(new Navigate(['admin-login']));
            case 'employer':
                return dispatch(new Navigate(['employer-login']));
            case 'user':
                return dispatch(new Navigate(['auth/login']));
            default:
                break;
        }

    }


    @Action(AutoLogin)
    autoLogin({patchState,dispatch}:StateContext<AppStateModel>){
        const user=sessionStorage.getItem('paygenie-user');
        if (typeof user ==='string'){
            patchState(
                {
                    user:new CustomersModel(JSON.parse(user)),
                    account_type:'user'
                }
            );
            return dispatch(
                new Navigate(['user'])
            )
        }
    }


    

}
