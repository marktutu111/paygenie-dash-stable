export class Toast {
    static readonly type = '[Toast] Toast]';
    constructor(public readonly data: { message:string,type: 'success' | 'error' }) {};
}
