import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { Toast } from './toast.action';
declare const swal:any;

interface StateModel {};

@State<StateModel>({
    name:'Toast',
    defaults: {
    }
})
@Injectable()
export class ToastState {

    @Action(Toast)
    toast ({}:StateContext<StateModel>,{ data }:Toast){
        const Toast = swal.mixin(
            {
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 6000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            }
        )

        Toast.fire(
            {
                icon:data.type,
                title:data.message
            }
        )
        
    };


};
