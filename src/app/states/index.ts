import { NgModule } from '@angular/core';
import { NgxsModule } from "@ngxs/store";
import { LoanService } from 'app/services/loans/loans.service';
import { LoansState } from './loans/loans.state';
import { HttpClientModule } from '@angular/common/http';
import { AppState } from './app/state';
import { EmployerState } from './employer/employer.state';
import { EmployerService } from 'app/services/employer/employer.service';
import { NgxsRouterPluginModule } from "@ngxs/router-plugin";
import { UserState } from './users/users.state';
import { UserService } from 'app/services/users/users.service';
import { BillPayService } from 'app/services/billpay/billpay.service';
import { BillpayState } from './billpay/billpay.state';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
import { AggregateService } from 'app/services/aggregate/aggregate.service';
import { ToastState } from './toast/toast.state';



@NgModule({
    declarations: [],
    imports: [
        HttpClientModule,
        NgxsModule.forRoot(
            [
                LoansState,
                AppState,
                EmployerState,
                UserState,
                BillpayState,
                ProgressBarState,
                ToastState
            ]
        ),
        NgxsRouterPluginModule.forRoot()
    ],
    exports: [],
    providers: [
        LoanService,
        EmployerService,
        UserService,
        BillPayService,
        AggregateService
    ],
})
export class StateModule {};