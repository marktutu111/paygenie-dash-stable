import { State, Selector, StateContext, Action } from '@ngxs/store';
import { BillPayService } from "../../services/billpay/billpay.service";
import { GetServices, GetServiceList, PayBill, GetCustomerBills } from './billpay.actions';
import { Toggle } from 'app/components/progress-bar/progress-bar.actions';
import { ServiceModel } from 'app/models/services/services.model';
import { UserService } from 'app/services/users/users.service';
import { BillModel } from 'app/models/bills/bills.model';
declare const swal:any;

interface StateModel {
    loading:false;
    services:any[],
    serviceList:any[] | object;
    bills:any[];
}

@State<StateModel>({
    name:'billpay',
    defaults: {
        loading:false,
        services:[],
        serviceList:[],
        bills:[]
    }
})
export class BillpayState {

    @Selector()
    static services (state:StateModel){
        return state.services;
    }

    @Selector()
    static serviceList (state:StateModel){
        return state.serviceList;
    }

    @Selector()
    static bills (state:StateModel){
        return state.bills;
    }


    constructor (private service:BillPayService,private userService:UserService) {};

    @Action(GetServices)
    async getService ({dispatch,patchState}:StateContext<StateModel>) {
        try {
            dispatch(new Toggle(true));
            const response=await this.service.getServices();
            if (response.success){
                const services = response.data.map(service=> new ServiceModel(service));
                patchState({services:services,serviceList:[]});
                return dispatch(
                    new Toggle(false)
                )
            } throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }

    
    @Action(GetServiceList)
    async getServiceList ({dispatch,patchState}:StateContext<StateModel>,{payload}:GetServiceList) {
        try {
            dispatch(new Toggle(true));
            patchState({serviceList:[]});
            const response=await this.service.getServiceList(payload);
            if (response.success){
                patchState({serviceList:response.data});
                return dispatch(
                    new Toggle(false)
                )
            } throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }



    @Action(PayBill)
    async paybill ({patchState,dispatch}:StateContext<StateModel>,{payload}:PayBill){
        try {
            dispatch(new Toggle(true));
            const {account_number,serviceDetails}=payload;
            console.log(account_number);
            const otp=await this.userService.sendOtp({account:account_number});
            if (otp.success){
                return swal({
                    title: 'Loan Settlement',
                    html: `<h4>You have requested to pay an amount of Ghs${serviceDetails.amount} to settle your bill, enter OTP received on your phone to confirm transaction.</h4>`,
                    icon: 'warning',
                    input: 'text',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, continue!'
                }).then(async (val) => {
                    if (typeof val === 'string' && val !== '') {
                        try {
                            payload['otp']=val;
                            const response = await this.service.payBill(payload);
                            if (response.success){
                                const {_id}=response.data;
                                swal({ html: `<h4>${response.message}</h4>`});
                                return dispatch(new Toggle(false));
                                // return this.getTransaction(_id,dispatch);
                            } throw Error(response.message);
                        } catch (err) {
                            dispatch(new Toggle(false));
                            swal({ html: `<h4>${err.message}</h4>`});
                        }
                    } else dispatch(new Toggle(false));
                }).catch(err=>dispatch(new Toggle(false)));
            } throw Error(otp.message)
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }


    getTransaction = async(id:string,dispatch=null)=>{
        try {
            const response=await this.userService.getTransaction(id);
            if (response.success){
                const  {transaction,status}=response.data;
                switch (transaction.code) {
                    case '03':
                        return this.getTransaction(id);
                    case '00':
                        if (typeof status === 'string'){
                            swal({title: 'Transaction Response',html: `<h4>${transaction['msg']}</h4>`});
                            if (dispatch) {
                                dispatch(new Toggle(false));
                            }
                            return;
                        }
                        return this.getTransaction(id);
                    default:
                        swal({title: 'Transaction Response',html: `<h4>${transaction['msg']}</h4>`});
                        break;
                }
            } throw Error(response.message);
        } catch (err) {
            return swal(
                {
                    title: 'Transaction Failed',
                    html: `<h4>${err.message}</h4>`,
                }
            )
        }
    }


    @Action(GetCustomerBills)
    async getCustomerBills ({dispatch,patchState}:StateContext<StateModel>,{customer}:GetCustomerBills){
        try {
            const response =await this.service.getBills(customer);
            if (response.success){
                const bills=response.data.map(d=>new BillModel(d));
                patchState({bills:bills});
                return dispatch(new Toggle(false));
            } throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }



}
