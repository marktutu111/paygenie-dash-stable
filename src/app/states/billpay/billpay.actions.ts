

export class GetServices {
    static readonly type = '[billpay] GetServices]';
}


export class GetServiceList {
    static readonly type = '[billpay] GetServiceList]';
    constructor(public readonly payload: {sid:string,serviceDetails?:any,kuwaita?:string}) {};
}


export class PayBill {
    static readonly type = '[billpay] PayBill]';
    constructor(public readonly payload: any) {}
}

export class GetCustomerBills {
    static readonly type = '[billpay] GetCustomerBills]';
    constructor(public readonly customer?: string) {};
}
