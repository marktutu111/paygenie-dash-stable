import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { UserService } from 'app/services/users/users.service';
import { Navigate } from "@ngxs/router-plugin";
import { SetUser, AccountType, Logout } from '../app/actions';
import { LoanModel } from 'app/models/loans/loans.model';
import { ResetPassword, UserLogin, GetUserLoans, UserLoanRequest, SelectLoan, UserLoanSummary, UpdateUser, ChangePin, ResetOTP, UpdatePassword, 
    PayLoan, 
    GetProfile, 
    SendOTP, 
    SendLoanRepayment,
    GetUsers,
    AdminUpdateUser,
    AdminBlockCustomer,
    GetUserSummary
} from './users.actions';
import { CustomersModel } from 'app/models/customers/customers.model';
import { Toggle } from 'app/components/progress-bar/progress-bar.actions';

declare const swal:any;


interface StateModel {
    loading: boolean;
    loans: any[];
    users:any[],
    selected: LoanModel;
    temp:any;
    profile:any;
    summary:any;
}


@State<StateModel>({
    name: 'UserState',
    defaults: {
        loading: false,
        loans: [],
        users:[],
        temp: null,
        selected: null,
        profile: null,
        summary:null
    }
})
export class UserState {


    @Selector()
    static loading (state: StateModel) {
        return state.loading;
    }

    @Selector()
    static summary (state: StateModel) {
        return state.summary;
    }

    @Selector()
    static temp (state: StateModel) {
        return state.temp;
    }

    @Selector()
    static users (state: StateModel) {
        return state.users;
    }


    @Selector()
    static profile (state: StateModel) {
        return state.profile;
    }

    @Selector()
    static selected (state: StateModel) {
        return state.selected;
    }


    @Selector()
    static loans (state: StateModel) {
        return state.loans;
    }


    constructor (private service: UserService, private store:Store) {};



    @Action(ResetOTP)
    async resetOTP ({ patchState, dispatch }: StateContext<StateModel>, { payload }: ResetOTP) {
        try {
            this.store.dispatch(new Toggle(true));
            const response = await this.service.sendOtp({account:payload['account']});
            if (response.success) {
                patchState(
                    {
                        temp:payload
                    }
                )
                return dispatch(
                    [
                        new Navigate(['customer/change-pin']),
                        new Toggle(false)
                    ]
                )
            }throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
            swal(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }


    @Action(SendOTP)
    async sendOTP ({dispatch,patchState}:StateContext<StateModel>,{payload}:SendOTP){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.sendOtp(payload);
            if (response.success){
                return dispatch(
                    new Toggle(false)
                )
            } throw Error(response.message);
        } catch (err) {
            swal({html: `<h4>${err.message}</h4>`});
            dispatch(new Toggle(false));
        }
    }

    @Action(AdminBlockCustomer)
    async adminBlockCustomer({patchState,dispatch}:StateContext<StateModel>,{payload}:AdminBlockCustomer){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.blockCustomer(payload);
            if(!response.success){
                throw Error(response.message);
            }
            dispatch(
                [
                    new Toggle(false),
                    new GetUsers()
                ]
            );
            return swal({html: `<h4>${response.message}</h4>`});
        } catch (err) {
            dispatch(new Toggle(false));
            swal({html: `<h4>${err.message}</h4>`});
        }
    }

    @Action(ResetPassword)
    async resetPassword ({ patchState, dispatch }: StateContext<StateModel>, { payload }: ResetPassword) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.resetPassword(payload);
            if (response.success) {
                patchState(
                    {
                        loading: false
                    }
                )
                swal({html: `<h4>${response.message}</h4>`});
                return dispatch(
                    new Navigate(['/auth/login'])
                )
            } else {
                patchState(
                    {
                        loading: false
                    }
                )
                return swal(
                    {
                        html: `<h4>${response.message}</h4>`,
                        confirmButtonColor: '#7eceff',
                        showCancelButton: true,
                        cancelButtonColor: 'red',
                        cancelButtonText: 'No, Don\'t Send',
                        confirmButtonText: 'Resend OTP'
                    }
                ).then((value) => {
                    if (value) {
                        return dispatch(
                            new ResetOTP(
                                { account: payload['account'] }
                            )
                        )
                    }
                })
            }
        } catch (err) {
            swal({html: `<h4>${err.message}</h4>`});
            patchState(
                {
                    loading: false
                }
            )
        }
    }



    @Action(UserLogin)
    async userLogin ({ patchState, dispatch }: StateContext<StateModel>, { payload }: UserLogin) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.login(payload);
            if (response.success) {
                const {active,mobile,empId}=response.data;
                switch (active) {
                    case true:
                        patchState({temp:null});
                        return dispatch(
                            [
                                new Toggle(false),
                                new AccountType('user'),
                                new Navigate(['user']),
                                new SetUser(new CustomersModel(response['data']))
                            ]
                        )
                    default:
                        return dispatch(
                            [
                                new Toggle(false),
                                new ResetOTP(
                                    {
                                        account:mobile,
                                        empId:empId['_id']
                                    }
                                )
                            ]
                        )
                }
            }throw Error(response.message);
        } catch (err) {
            swal({ html: `<h4>${err.message}</h4>`});
            dispatch(new Toggle(false));
        }
    }



    @Action(GetUserLoans)
    async getUserLoans ({ patchState,dispatch }: StateContext<StateModel>, { payload,loader }: GetUserLoans) {
        try {

            loader && dispatch(new Toggle(true));
            const response = await this.service.getloans(payload);
            if (response.success) {
                const loans = response.data.map(loan => new LoanModel(loan));
                loader && dispatch(new Toggle(false));
                return patchState(
                    {
                        loans: loans
                    }
                )
            }
            throw Error(response['message']);
        } catch (err) {
            loader && dispatch(new Toggle(false));
        }
    }

    @Action(GetUsers)
    async getUsers ({ patchState,dispatch }: StateContext<StateModel>) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.getUsers();
            if (response.success) {
                const customers = response.data.map(customer => new CustomersModel(customer));
                dispatch(new Toggle(false));
                return patchState(
                    {
                        users: customers
                    }
                )
            }
            throw Error(response['message']);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }

    @Action(GetProfile)
    async getProfile ({patchState,dispatch}:StateContext<StateModel>,{id}:GetProfile) {
        try {
            dispatch(new Toggle(true));
            const response =await this.service.getProfile(id);
            if (response.success) {
                dispatch(new Toggle(false));
                return patchState(
                    {
                        profile:new CustomersModel(response.data)
                    }
                )
            } throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }



    @Action(UserLoanRequest)
    async userLoanRequest ({ patchState,dispatch }: StateContext<StateModel>, { payload }: GetUserLoans) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.loanRequest(payload);
            if (!response.success){
                throw Error(response.message);
            }
            const {_id}=response.data;
            patchState({loading: false});
            swal({ html: `<h4>${response['message']}</h4>` });
            return dispatch(new Toggle(false));
        } catch (err) {
            dispatch(new Toggle(false));
            return swal(
                {
                    html: `<h4>${err['message']}</h4>`
                }
            )
        }
    }



    @Action(UserLoanSummary)
    async loanSummary ({ patchState,dispatch }: StateContext<StateModel>, { payload }: UserLoanSummary) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.getloans(payload);
            if (response && response['code'] === '200') {
                return dispatch(new Toggle(false));
            }
            throw Error(response['msg']);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }


    @Action(UpdatePassword)
    async updatePassword ({patchState,dispatch}:StateContext<StateModel>,{payload}:UpdatePassword) {
        try {
            patchState({loading:true});
            const response = await this.service.updatePassword(payload);
            if (response.success) {
                swal({ html: `<h4>Password reset successful</h4>`});
                patchState(
                    {
                        loading:false
                    }
                )
                return dispatch(
                    new Logout()
                )
            } throw Error(response.message);
        } catch (err) {
            swal({ html: `<h4>${err.message}</h4>`});
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(GetUserSummary)
    async getUserSummary({patchState,dispatch}:StateContext<StateModel>,{id}:GetUserSummary){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.getUserSummary(id);
            if(!response.success){
                throw Error(response.message);
            }
            dispatch(new Toggle(false));
            patchState(
                {
                    summary:response.data[0]
                }
            )
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(PayLoan)
    async payload ({patchState,dispatch}: StateContext<StateModel>, {payload}:PayLoan) {
        try {
            dispatch(new Toggle(true));
            const {account_number,amount}=payload;
            const otp=await this.service.sendOtp({account:account_number});
            if (otp.success){
                return swal({
                    title: 'Loan Settlement',
                    html: `<h4>You have requested to pay an amount of ${amount} to settle your loan with PayGenie, enter OTP received on your phone to confirm transaction.</h4>`,
                    icon: 'warning',
                    input: 'text',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, continue!'
                }).then(async (val) => {
                    if (typeof val === 'string' && val !== '') {
                        try {
                            payload['otp']=val;
                            const response = await this.service.payLoan(payload);
                            if (response.success){
                                const {_id}=response.data;
                                swal({ html: `<h4>${response.message}</h4>`});
                                patchState({loading:false});
                                return dispatch(new Toggle(false));
                            } throw Error(response.message);
                        } catch (err) {
                            dispatch(new Toggle(false));
                            swal(
                                { 
                                    html: `<h4>${err.message}</h4>`
                                }
                            );
                        }
                    }
                }).catch(err=>dispatch(new Toggle(false)));
            } throw Error(otp.message)
        } catch (err) {
            dispatch(new Toggle(false));
            swal({ html: `<h4>${err.message}</h4>`});
        }
    }

    @Action(UpdateUser)
    async updateUser ({ patchState, dispatch }: StateContext<StateModel>, { payload }: UpdateUser) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.updateUser(payload);
            if (response.success) {
                swal({ html: `<h4>Account updated successful</h4>`});
                dispatch(new Toggle(false));
                return dispatch(new SetUser(new CustomersModel(response.data)));
            }
            throw Error(response['message']);
        } catch (err) {
            swal({ html: `<h4>${err.message}</h4>`});
            patchState(
                {loading: false}
            )
        }
    }


    @Action(AdminUpdateUser)
    async adminUpdateUser ({ patchState, dispatch }: StateContext<StateModel>, { payload }: AdminUpdateUser) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.updateUser(payload);
            if (response.success) {
                swal({ html: `<h4>Account updated successful</h4>`});
                dispatch(new Toggle(false));
                return dispatch(
                    new GetUsers()
                )
            }
            throw Error(response['message']);
        } catch (err) {
            swal({ html: `<h4>${err.message}</h4>`});
            patchState(
                {loading: false}
            )
        }
    }



    @Action(ChangePin)
    async changePin ({ patchState,dispatch }: StateContext<StateModel>, { payload }: ChangePin) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.changePin(payload);
            if (response && response['code'] === '200') {
                dispatch(new Toggle(false));
                return swal(
                    {
                        html: `<h4>${response['msg']}</h4>`
                    }
                )
            }

            throw Error(response['msg']);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({html: `<h4>${err['message']}</h4>`});
        }
    }


    @Action(SelectLoan)
    selectedLoan ({ patchState }: StateContext<StateModel>, { payload }: SelectLoan) {
        patchState(
            {
                selected: payload
            }
        )
    }


    getTransaction = async(id:string)=>{
        try {
            const response=await this.service.getTransaction(id);
            if (response.success){
                const  {transaction,status,customer}=response.data;
                switch (transaction.code) {
                    case '03':
                        return this.getTransaction(id);
                    case '00':
                        typeof status === 'string' ? swal({title: 'Transaction Response',html: `<h4>${transaction['msg']}</h4>`}):this.getTransaction(id);
                        break;
                    default:
                        swal({title: 'Transaction Response',html: `<h4>${transaction['msg']}</h4>`});
                        break;
                }
            } throw Error(response.message);
        } catch (err) {
            return swal(
                {
                    title: 'Transaction Failed',
                    html: `<h4>${err.message}</h4>`,
                }
            )
        }
    }


};
