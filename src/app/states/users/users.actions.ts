

export class ResetPassword {
    static readonly type = '[UserState] ResetPassword]';
    constructor(public readonly payload: any) {}
}



export class UserLogin {
    static readonly type = '[UserState] UserLogin]';
    constructor(public readonly payload: any) {};
}




export class GetUserLoans {
    static readonly type = '[UserState] GetUserLoans]';
    constructor(public readonly payload:string,public readonly loader=true) {}
}


export class GetUsers {
    static readonly type = '[UserState] GetUsers]';
    constructor() {};
}



export class GetProfile {
    static readonly type = '[UserState] GetProfile]';
    constructor(public readonly id: string) {}
}


export class GetUserSummary {
    static readonly type = '[UserState] GetUserSummary]';
    constructor(public readonly id: string) {}
}



export class UserLoanRequest {
    static readonly type = '[UserState] UserLoanRequest]';
    constructor(public readonly payload: any) {};
}



export class SelectLoan {
    static readonly type = '[UserState] SelectLoan]';
    constructor(public readonly payload: any) {}
}


export class UserLoanSummary {
    static readonly type = '[UserState] UserLoanAggregator]';
    constructor(public readonly payload: any) {}
}



export class UpdateUser {
    static readonly type = '[UserState] UpdateUser]';
    constructor(public readonly payload: any) {}
}


export class AdminUpdateUser {
    static readonly type = '[UserState] UpdateUser]';
    constructor(public readonly payload: any) {}
}


export class AdminBlockCustomer {
    static readonly type = '[UserState] AdminBlockCustomer]';
    constructor(public readonly payload:{id:string,blocked:boolean}) {};
}

export class SendOTP {
    static readonly type = '[UserState] SendOTP]';
    constructor(public readonly payload: { account:string }) {}
}

export class SendLoanRepayment {
    static readonly type = '[UserState] SendLoanRepayment]';
    constructor(public readonly payload?: any) {}
}


export class ChangePin {
    static readonly type = '[UserState] ChangePin]';
    constructor(public readonly payload: any) {}
}


export class ResetOTP {
    static readonly type = '[UserState] ResetOTP]';
    constructor(public readonly payload: any) {}
}


export class UpdatePassword {
    static readonly type = '[UserState] UpdatePassword]';
    constructor(public readonly payload?: any) {};
}


export class PayLoan {
    static readonly type = '[UserState] PayLoan]';
    constructor(public readonly payload?: any) {};
}
