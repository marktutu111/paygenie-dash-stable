import { State, Action, Selector, StateContext } from '@ngxs/store';
import { LoanService } from 'app/services/loans/loans.service';
import { GetLoans, AppForLoan, GetLoanSettings, SetSettings, GetEmployerLoanSummary, GetLoanTransactions } from './loans.actions';
import { LoanModel } from 'app/models/loans/loans.model';
import { Toggle } from 'app/components/progress-bar/progress-bar.actions';
import { SettingsModel } from 'app/models/settings/settings.model';
declare const swal:any;

interface StateModel {
    loading: Boolean;
    loans: any[];
    settings:any[];
    loanSummary:any;
    transactions:any[];
}


@State<StateModel>({
    name: 'Loans',
    defaults: {
        loading: false,
        loans: [],
        settings:[],
        loanSummary:null,
        transactions:[]
    }
})
export class LoansState {

    @Selector()
    static loading (state: StateModel) {
        return state.loading;
    }

    @Selector()
    static transactions (state: StateModel) {
        return state.transactions;
    }

    @Selector()
    static summary (state: StateModel) {
        return state.loanSummary;
    }

    @Selector()
    static settings (state: StateModel) {
        return state.settings;
    }

    @Selector()
    static loans (state: StateModel) {
        return state.loans;
    }

    constructor (private service: LoanService) {};


    @Action(GetEmployerLoanSummary)
    async getEmployerLoanSummary({patchState,dispatch}:StateContext<StateModel>,{id}:GetEmployerLoanSummary){
        try {
            const response=await this.service.getEmployerSummary(id);
            if(!response.success){
                throw Error(response.message);
            }
            patchState(
                {
                    loanSummary:response.data[0]
                }
            )
        } catch (err) {
            
        }
    }


    @Action(GetLoanSettings)
    async getLoanSettings ({patchState,dispatch}:StateContext<StateModel>){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.getSettings();
            if (!response.success){
                throw Error(response.message);
            }
            const settings=response.data.map(s=>new SettingsModel(s));
            patchState(
                {
                    settings:settings
                }
            )
            dispatch(new Toggle(false));
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }

    @Action(GetLoanTransactions)
    async getLoanTransactions({patchState,dispatch}:StateContext<StateModel>,{id}:GetLoanTransactions){
        try {
            dispatch(new Toggle(true));
            patchState({transactions:[]});
            const response=await this.service.getLoanTransactions(id);
            if(!response.success){
                throw Error(response.message);
            }
            patchState(
                {
                    transactions:response.data
                }
            )
            dispatch(new Toggle(false));
        } catch (err) {
            dispatch(
                new Toggle(false)
            );
        }
    }

    @Action(SetSettings)
    async setSettings ({patchState,dispatch}:StateContext<StateModel>,{data}:SetSettings){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.setSettings(data);
            if (!response.success){
                throw Error(response.message);
            }
            swal(
                {
                    html: `<h4>Settings updated successful</h4>`
                }
            )
            dispatch(
                [
                    new Toggle(false),
                    new GetLoanSettings()
                ]
            );
        } catch (err) {
            dispatch(new Toggle(false));
            swal(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }


    @Action(GetLoans)
    async getLoans ({ patchState,dispatch }: StateContext<StateModel>) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.getLoans();
            if (response.success) {
                const loans = response.data.map(loan => new LoanModel(loan));
                patchState({loans: loans});
                return dispatch(new Toggle(false));
            }
            throw Error(response['msg']);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }


    @Action(AppForLoan)
    async applyForLoan ({ patchState }: StateContext<StateModel>, { data }: AppForLoan) {
        try {
            
            patchState(
                {
                    loading: true
                }
            )
            const response = await this.service.applyLoan(data);
            patchState(
                {
                    loading: false
                }
            )
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


}
