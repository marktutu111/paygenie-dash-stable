import { GetLoansInterfaceModel, ApplyLoanInterfaceModel } from "app/models/loans/loans.model";


export class GetLoans {
    static readonly type = '[Loans] GetLoans';
}


export class AppForLoan {
    static readonly type = '[Loans] AppForLoan';
    constructor(public readonly data: ApplyLoanInterfaceModel) {};
}

export class GetLoanSettings {
    static readonly type = '[Loans] GetLoanSettings]';
}


export class SetSettings {
    static readonly type = '[Loans] SetSettings]';
    constructor(public readonly data: any) {};
}


export class GetEmployerLoanSummary {
    static readonly type = '[Loans] GetEmployerLoanSummary]';
    constructor(public readonly id:string) {};
}


export class GetLoanTransactions {
    static readonly type = '[Loans] GetLoanTransactions]';
    constructor(public readonly id:string) {};
}
