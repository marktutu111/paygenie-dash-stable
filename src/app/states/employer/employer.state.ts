import { State, Selector, Action, StateContext, Store } from '@ngxs/store';
import { EmployerService } from 'app/services/employer/employer.service';
import { GetEmployerLoanApplicants, GetEmployerCustomers, EmployerUploadCustomer, EmployerDeleteCustomer, 
    EmployerUpdateCustomer, 
    EmployerAuthenticate, 
    EmployerUploadBulk, 
    FilterByDate, 
    FilterByPaymentsDue, 
    EmployerLoanAggregator, 
    EmployerNewCustomer, 
    FetchEmployer,
    AdminGetEmployers,
    AdminUpdateEmployer,
    AdminAddEmployer,
    AdminEmployerPayLoan,
    AdminGetInvoice,
    AdminAuthenticate,
    ApproveCustomer,
    ApproveEmployer,
    EmployerChangePassword,
    EmployerApproveAll
} from './employer.actions';
import swal from 'sweetalert2';
import { Navigate } from "@ngxs/router-plugin";
import { AccountType, GetAdminSummary, Logout, SetUser } from '../app/actions';
import { LoanModel } from 'app/models/loans/loans.model';
import { CustomersModel } from 'app/models/customers/customers.model';
import { AppState } from '../app/state';
import * as shortid from "shortid";
import { Toggle } from 'app/components/progress-bar/progress-bar.actions';
import { Toast } from '../toast/toast.action';
import { EmployerModel } from 'app/models/employer/employer.model';
import { LoanService } from 'app/services/loans/loans.service';



interface StateModel {
    loading: boolean;
    loans: any[];
    customers: any[];
    employers:any[];
    loanSummary: any;
    invoices:any[];
    profile:any;
}


@State<StateModel>({
    name: 'Employer',
    defaults: {
        loading: false,
        loans: [],
        employers:[],
        profile: {},
        customers: [],
        invoices:[],
        loanSummary: {
            total_customers:0,
            count:0,
            amountBorrowedWithInterest:0,
            amountRemaining:0
        }
    }
})
export class EmployerState {

    @Selector()
    static loading (state: StateModel) {
        return state.loading;
    }

    @Selector()
    static employers (state: StateModel) {
        return state.employers;
    }

    @Selector()
    static invoices (state: StateModel) {
        return state.invoices;
    }

    @Selector()
    static profile (state: StateModel) {
        return state.profile;
    }


    @Selector()
    static customers (state: StateModel) {
        return state.customers;
    }


    @Selector()
    static loadSummary (state: StateModel) {
        return state.loanSummary;
    }


    @Selector()
    static loans (state: StateModel) {
        return state.loans;
    }

    
    constructor (
        private service:EmployerService, 
        private store:Store,
        private loanService:LoanService
    ) {};


    @Action(GetEmployerLoanApplicants)
    async getEmployerLoansApplicants ({ patchState }: StateContext<StateModel>, { payload,loader }: GetEmployerLoanApplicants) {
        try {
            loader && patchState({ loading: true });
            const response = await this.service.getEmployerLoansApplicants(payload);
            if (response.success) {
                const loans = response.data.map(loan => new LoanModel(loan));
                return patchState(
                    {
                        loading: false,
                        loans: loans
                    }
                )
            }

            throw Error(response['message']);
        } catch (err) {
            loader && patchState(
                {
                    loading: false
                }
            )
            
        }
    }


    @Action(EmployerChangePassword)
    async changePassword({patchState,dispatch}:StateContext<StateModel>,{payload}:EmployerChangePassword){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.changePassword(payload);
            if(!response.success){
                throw Error(response.message);
            }
            swal({
                html: `<h4>${response.message}</h4>`
            }).catch(err => null);
            dispatch(
                [
                    new Toggle(false),
                    new Logout()
                ]
            )
        } catch (err) {
            swal({
                html: `<h4>${err.message}</h4>`
            }).catch(err => null);
            dispatch(
                new Toggle(false)
            )
        }
    }


    @Action(AdminGetEmployers)
    async adminGetEmployers ({ patchState,dispatch }: StateContext<StateModel>) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.getEmployers();
            if (response.success) {
                dispatch(new Toggle(false));
                return patchState(
                    {
                        employers:response.data.map(emp=>new EmployerModel(emp,this.loanService))
                    }
                )
            }

            throw Error(response.msg);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }


    @Action(AdminGetInvoice)
    async adminGetInvoice ({ patchState,dispatch }: StateContext<StateModel>,{id}:AdminGetInvoice) {
        try {
            dispatch(new Toggle(true));
            const response:any=typeof id=='string' ? await this.service.getInvoiceById(id): await this.service.getInvoice();
            if (response.success) {
                dispatch(new Toggle(false));
                return patchState(
                    {
                        invoices:response.data
                    }
                )
            }

            throw Error(response.msg);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }


    @Action(EmployerUploadCustomer)
    async employerUploadCustomer ({ patchState,dispatch }: StateContext<StateModel>, { payload }: EmployerUploadCustomer) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.addCustomer(payload);
            if (response && response['code'] === '200') {
                dispatch(new Toggle(false));
                return swal(
                    {
                        title: `success`,
                        text: 'Customer Uploaded successfully'
                    }
                )
            }
            throw Error(response['msg']);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({
                html: `<h4>${err.message}</h4>`
            }).catch(err => null);
        }
    }


    @Action(EmployerDeleteCustomer)
    async employerDeleteCustomer ({ patchState,dispatch }: StateContext<StateModel>, { payload }: EmployerDeleteCustomer) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.deleteCustomer(payload);
            if (response && response['code'] === '200') {
                dispatch(new Toggle(false));
                return swal(
                    {
                        title: `success`,
                        text: 'Customer Deleted successfully'
                    }
                )

            }
            throw Error(response['msg']);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({
                html: `<h4>${err.message}</h4>`
            }).catch(err => null);
        }
    }


    @Action(ApproveEmployer)
    async approveEmployer ({patchState,dispatch}:StateContext<StateModel>,{payload}:ApproveEmployer){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.approveEmployer(payload);
            if(!response.success){
                throw Error(response.message);
            }
            dispatch(
                [
                    new Toggle(false),
                    new AdminGetEmployers()
                ]
            );
            return swal(
                {
                    title:`success`,
                    text:response.message
                }
            )
        } catch (err) {
            dispatch(new Toggle(false));
            swal({
                html: `<h4>${err.message}</h4>`
            }).catch(err => null);
        }
    }


    @Action(EmployerApproveAll)
    async employerApproveAll({patchState,dispatch}:StateContext<StateModel>,{payload}:EmployerApproveAll){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.approveAll(payload);
            if(!response.success){
                throw Error(response.message);
            }
            dispatch(new Toggle(false));
            return swal({
                html: `<h4>${response.message}</h4>`
            }).catch(err => null);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({
                html: `<h4>${err.message}</h4>`
            }).catch(err => null);
        }
    }


    @Action(GetEmployerCustomers)
    async getEmployerCustomers ({patchState,dispatch}:StateContext<StateModel>,{payload,loader}:GetEmployerCustomers){
        try {
            loader && dispatch(new Toggle(true));
            const response =await this.service.getCustomers(payload);
            if (response.success){
                const customers=response.data.map(customer => new CustomersModel(customer));
                patchState(
                    {
                        customers:customers
                    }
                )
                return loader && dispatch(new Toggle(false));
            }
        } catch (err) {
            loader && dispatch(new Toggle(false));
        }
    }


    @Action(EmployerUpdateCustomer)
    async employerUpdateCustomer ({ patchState, dispatch }: StateContext<StateModel>, { payload }: EmployerUpdateCustomer) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.updateCustomer(payload);
            if (response.success) {
                const {_id} = this.store.selectSnapshot(AppState.user);
                dispatch(
                    [
                        new GetEmployerCustomers(_id),
                        new Toggle(false)
                    ]
                )
                return swal(
                    {
                        title: `success`,
                        text: response.message
                    }
                )

            }

            throw Error(response['message']);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({
                html: `<h4>${err.message}</h4>`
            }).catch(err => null);
            
        }
    }


    @Action(ApproveCustomer)
    async approveCustomer({dispatch,patchState}:StateContext<StateModel>,{payload}:ApproveCustomer){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.approveCustomer(payload);
            if(!response.success){
                throw Error(response.message);
            }
            dispatch(new Toggle(false));
            return swal(
                {
                    title: `success`,
                    text: response.message
                }
            )
        } catch (err) {
            dispatch(new Toggle(false));
            return swal(
                {
                    title: `error`,
                    text: err.message
                }
            )
        }
    }

    @Action(AdminUpdateEmployer)
    async adminUpdateEmployer ({ patchState, dispatch }: StateContext<StateModel>, { payload }: AdminUpdateEmployer) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.updateEmployer(payload);
            if (response.success) {
                dispatch(
                    [
                        new Toggle(false),
                        new AdminGetEmployers()
                    ]
                );
                return swal(
                    {
                        title: `success`,
                        text: response.message
                    }
                )
            }

            throw Error(response['message']);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({
                html: `<h4>${err.message}</h4>`
            }).catch(err => null);
            
        }
    }

    @Action(FetchEmployer)
    async fetchEmployer ({patchState,dispatch}:StateContext<StateModel>,{id}:FetchEmployer) {
        try {
            patchState({loading:true});
            const response=await this.service.getEmployer(id);
            if (response && response.success){
                return patchState(
                    {
                        loading:false,
                        profile:response.data
                    }
                )
            }
        } catch (err) {
        }
    }


    @Action(EmployerAuthenticate)
    async employerAuthenticate ({ patchState, dispatch }: StateContext<StateModel>, { payload }: EmployerAuthenticate) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.authenticateEmployer(payload);
            if (response.success) {
                patchState(
                    {
                        loading: false
                    }
                );
                return dispatch(
                    [
                        new AccountType('employer'),
                        new Navigate(['employer']),
                        new SetUser(response.data)
                    ]
                )
            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            swal(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
            
        }
    }


    @Action(AdminAuthenticate)
    async adminAuthenticate ({ patchState, dispatch }: StateContext<StateModel>, { payload }: AdminAuthenticate) {
        try {
            patchState({loading: true})
            const response = await this.service.adminLogin(payload);
            if (response.success) {
                patchState({loading: false});
                return dispatch(
                    [
                        new AccountType('admin'),
                        new Navigate(['admin']),
                        new SetUser(response.data)
                    ]
                )
            }
            throw Error(response.message);
        } catch (err) {
            patchState({ loading: false});
            swal({html: `<h4>${err.message}</h4>`});
        }
    }



    @Action(EmployerUploadBulk)
    async uploadUploadBulk ({ patchState }: StateContext<StateModel>, { payload }: EmployerUploadBulk) {
        patchState({loading: true});
        payload.forEach(async(customer:any[],i:number) => {
            const size:number =customer.length;
            try {
                patchState({loading:true});
                const response = await this.service.addCustomer(customer);
                if (response.success) {
                    return patchState({loading:false});
                } throw Error(response.message);
            } catch (err) {
                patchState({loading:false})
            }
        });
    }


    @Action(EmployerNewCustomer)
    async employerNewCustomer ({patchState,dispatch}:StateContext<StateModel>,{payload}:EmployerNewCustomer) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.addCustomer(payload);
            if (response.success) {
                swal({html: `<h4>${response.message}</h4>`});
                return dispatch(
                    [
                        new Toggle(false),
                        new GetAdminSummary()
                    ]
                );
            } throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({html: `<h4>${err.message}</h4>`});
        }
    }


    @Action(AdminAddEmployer)
    async adminAddEmployer ({patchState,dispatch}:StateContext<StateModel>,{payload}:AdminAddEmployer) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.addEmployer(payload);
            if (response.success) {
                swal({html: `<h4>${response.message}</h4>`});
                return dispatch(
                    [
                        new Toggle(false),
                        new GetAdminSummary()
                    ]
                );
            } throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({html: `<h4>${err.message}</h4>`});
        }
    }


    @Action(AdminEmployerPayLoan)
    async adminEmployerPayLoan ({patchState,dispatch}:StateContext<StateModel>,{payload}:AdminEmployerPayLoan) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.payInvoice(payload);
            if (response.success) {
                swal({html: `<h4>${response.message}</h4>`});
                return dispatch(
                    [
                        new Toggle(false),
                        new GetAdminSummary()
                    ]
                );
            } throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
            swal({html: `<h4>${err.message}</h4>`});
        }
    }


    @Action(FilterByDate)
    async filterByDate ({ patchState,dispatch }: StateContext<StateModel>, { payload }: FilterByDate) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.filterByDate(payload);
            if (response.success) {
                const loans = response.data.map(loan => new LoanModel(loan));
                patchState({loans: loans});
                return dispatch(new Toggle(false));
            }
            throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }


    @Action(FilterByPaymentsDue)
    async filterByPaymentsDue ({ patchState ,dispatch}: StateContext<StateModel>, { payload }: FilterByPaymentsDue) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.filterByPaymentsDue(payload);
            if (response.success) {
                const loans = response.data.map(loan => new LoanModel(loan));
                patchState(
                    {
                        loans: loans
                    }
                )
                return dispatch(new Toggle(false));
            }

            throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }



    @Action(EmployerLoanAggregator)
    async employerLoanAggregator ({ patchState }: StateContext<StateModel>, { payload }: EmployerLoanAggregator) {
        try {

            const response = await this.service.loanAggregate(payload);
            if (response && response['code'] === '200') {
                patchState(
                    {
                        loanSummary: response
                    }
                )
            }
            
        } catch (err) {
            
        }
    }




}
