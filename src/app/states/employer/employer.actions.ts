

export class GetEmployerLoanApplicants {
    static readonly type = '[Employer] GetEmployerLoanApplicants';
    constructor(public readonly payload: any,public readonly loader:boolean=true) {}
}



export class GetEmployerCustomers {
    static readonly type = '[Employer] GetEmployerCustomers';
    constructor(public readonly payload: any,public loader:boolean=true) {}
}


export class AdminGetEmployers {
    static readonly type = '[Employer] AdminGetEmployers';
}

export class AdminUpdateEmployer {
    static readonly type = '[Employer] AdminUpdateEmployer';
    constructor(public readonly payload?: any) {}
}


export class AdminAddEmployer {
    static readonly type = '[Employer] AdminAddEmployer';
    constructor(public readonly payload?: any) {}
}



export class ApproveEmployer {
    static readonly type = '[Employer] ApproveEmployer';
    constructor(public readonly payload?: any) {}
}

export class AdminEmployerPayLoan {
    static readonly type = '[Employer] AdminEmployerPayLoan';
    constructor(public readonly payload?: any) {}
}


export class FetchEmployer {
    static readonly type = '[Employer] FetchEmployer';
    constructor(public readonly id: string) {}
}


export class EmployerUploadCustomer {
    static readonly type = '[Employer] EmployerUploadCustomer';
    constructor(public readonly payload?: any) {}
}


export class EmployerDeleteCustomer {
    static readonly type = '[Employer] EmployerDeleteCustomer';
    constructor(public readonly payload?: any) {}
}


export class EmployerUpdateCustomer {
    static readonly type = '[Employer] EmployerUpdateCustomer';
    constructor(public readonly payload: any) {};
}

export class EmployerNewCustomer {
    static readonly type = '[Employer] EmployerNewCustomer';
    constructor(public readonly payload: any) {};
}


export class EmployerAuthenticate {
    static readonly type = '[Employer] EmployerAuthenticate]';
    constructor(public readonly payload: any) {}
}


export class AdminAuthenticate {
    static readonly type = '[Employer] AdminAuthenticate]';
    constructor(public readonly payload: any) {}
}


export class EmployerUploadBulk {
    static readonly type = '[Employer] EmployerUploadBulk]';
    constructor(public readonly payload: any) {};
}



export class FilterByDate {
    static readonly type = '[Employer] FilterByDate';
    constructor(public readonly payload: any) {}
}

export class ApproveCustomer {
    static readonly type = '[Employer] ApproveCustomer';
    constructor(public readonly payload:{ id:string,approved:boolean }) {}
}


export class FilterByPaymentsDue {
    static readonly type = '[Employer] FilterByPaymentsDue]';
    constructor(public readonly payload: any) {}
}



export class EmployerLoanAggregator {
    static readonly type = '[Employer] EmployerLoanAggregator]';
    constructor(public readonly payload: any) {}
}


export class AdminGetInvoice {
    static readonly type = '[Employer] AdminGetInvoice';
    constructor(public readonly id?:string) {}
}


export class EmployerChangePassword {
    static readonly type = '[Employer] EmployerChangePassword]';
    constructor(public readonly payload:any) {};
}


export class EmployerApproveAll {
    static readonly type = '[Employer] EmployerApproveAll]';
    constructor(public readonly payload?:any) {}
}
