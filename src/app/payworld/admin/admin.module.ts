import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { MdModule } from 'app/md/md.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/app.module';
import { MatNativeDateModule } from '@angular/material';
import { SidebarModule } from 'app/sidebar/sidebar.module';
import { NavbarModule } from 'app/shared/navbar/navbar.module';
import { FooterModule } from 'app/shared/footer/footer.module';
import { FixedpluginModule } from 'app/shared/fixedplugin/fixedplugin.module';
import { AdminAddCustomerComponent } from './admin-add-customer/admin-add-customer.component';
import { AdminCustomerListComponent } from './admin-customer-list/admin-customer-list.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { CustomerLoansComponent } from './customer-loans/customer-loans.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProgressBarModule } from 'app/components/progress-bar/progress-bar.module';
import { AdminEmployerListComponent } from "./admin-employer-list/admin-employer-list.component";
import { AdminAddEmployerComponent } from './admin-add-employer/admin-add-employer.component';
import { AdminPayInvoiceComponent } from './admin-pay-invoice/admin-pay-invoice.component';
import { AdminInvoiceListComponent } from './admin-invoice-list/admin-invoice-list.component';
import { SettingsComponent } from './settings/settings.component';
import { AdminUploadCustomers } from './admin-upload-customer/admin-upload-customer.component';



const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {
                path:'',
                redirectTo:'loans',
                pathMatch:'full'
            },
            {
                path:'loans',
                component:CustomerLoansComponent
            },
            {
                path:'customers',
                component:AdminCustomerListComponent
            },
            {
                path:'employers',
                component:AdminEmployerListComponent
            },
            {
                path:'addcustomer',
                component:AdminAddCustomerComponent
            },
            {
                path:'uploadcustomers',
                component:AdminUploadCustomers
            },
            {
                path:'addemployer',
                component:AdminAddEmployerComponent
            },
            {
                path:'payinvoice',
                component:AdminPayInvoiceComponent
            },
            {
                path:'invoices',
                component:AdminInvoiceListComponent
            },
            {
                path:'settings',
                component:SettingsComponent
            }
        ]
    }
]




@NgModule({
    declarations: [
        AdminComponent,
        AdminAddCustomerComponent,
        AdminCustomerListComponent,
        AdminDashboardComponent,
        CustomerLoansComponent,
        AdminEmployerListComponent,
        AdminAddEmployerComponent,
        AdminPayInvoiceComponent,
        AdminInvoiceListComponent,
        SettingsComponent,
        AdminUploadCustomers
    ],
    imports: [ 
        CommonModule,
        MdModule,
        FormsModule,
        MaterialModule,
        MatNativeDateModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedpluginModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        ProgressBarModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class AdminModule {

}