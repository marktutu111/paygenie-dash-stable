import { Component, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { CustomersModel } from 'app/models/customers/customers.model';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
import { AdminGetInvoice } from 'app/states/employer/employer.actions';
import { LoanModel, LoansInterfaceModel } from 'app/models/loans/loans.model';


declare const $: any;



@Component({
    selector: 'admin-invoice-list',
    templateUrl: './admin-invoice-list.component.html',
    styleUrls: ['./admin-invoice-list.component.scss']
})
export class AdminInvoiceListComponent implements OnInit {

    filter: CustomersModel[] = [];
    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.invoices) invoices$: Observable<any[]>;
    

    @ViewChild('dataTable', null) table;
    dataTable: any;

    p: number = 1;
    loans:LoanModel[]=[];
    selectedId:string=null;
    code:string;
    onsearch:boolean = false;

    constructor(private store: Store) {};


    ngOnInit(): void {
        this.store.dispatch(new AdminGetInvoice());
    };


    onSearch (e) {
        try {
            const text: string = e.target.value;
            if (text.length > 0) {
                this.onsearch = true;
                const invoices = this.store.selectSnapshot(EmployerState.invoices);
                this.filter = [];
                invoices.forEach((invoice,i) => {
                    const id=invoice.invoiceId;
                    const employer=invoice.employer;
                    if (id.indexOf(text) > -1 || employer.indexOf(text) > -1) {
                        this.filter.push(invoice);
                    }
                });
            } else this.onsearch = false;
        } catch (err) {
            this.onsearch = false;
        }
    }


    onSelected(invoice){
        this.loans=invoice.loans.map(loan=>new LoanModel(loan));
        this.selectedId=invoice._id;
    }




}
