import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { AdminAddEmployer } from 'app/states/employer/employer.actions';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
declare const swal:any;

@Component({
    selector: 'app-admin-add-customer',
    templateUrl: './admin-add-employer.component.html',
    styleUrls: ['./admin-add-employer.component.scss']
})
export class AdminAddEmployerComponent implements OnInit {


    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.employers) employers$:Observable<any[]>;
    formGroup: FormGroup;

    constructor(private store: Store) {};

    
    ngOnInit(): void {
        this.formGroup = new FormGroup(
            {
                name: new FormControl(null,Validators.required),
                mobile: new FormControl(null, Validators.required),
                email: new FormControl(null, Validators.required),
                location:new FormControl(null,Validators.required),
                digital_address:new FormControl(null,Validators.required),
                address:new FormControl(null,Validators.required)
            }
        )

    };



    save () {
        if (this.formGroup.valid) {
            return swal({
                title: 'New Employer',
                html: `<h4>You have requested to add a new Employer Account</h4>`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, continue!'
            }).then((val) => {
                if (val) {
                    const data=this.formGroup.value;
                    this.store.dispatch(
                        new AdminAddEmployer(data)
                    ).subscribe(()=>this.formGroup.reset());
                }
            }).catch(err=>null);
        }
    }




}
