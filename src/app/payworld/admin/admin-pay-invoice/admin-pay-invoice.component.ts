import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { AdminEmployerPayLoan } from 'app/states/employer/employer.actions';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
declare const swal:any;

@Component({
    selector: 'app-admin-pay-invoice',
    templateUrl: './admin-pay-invoice.component.html',
    styleUrls: ['./admin-pay-invoice.component.scss']
})
export class AdminPayInvoiceComponent implements OnInit {


    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.employers) employers$:Observable<any[]>;
    formGroup: FormGroup;

    constructor(private store: Store) {};

    
    ngOnInit(): void {
        this.formGroup = new FormGroup(
            {
                invoiceId: new FormControl(null,Validators.required),
                employer: new FormControl(null, Validators.required),
                amount: new FormControl(null, Validators.required),
                date:new FormControl(Date.now(),Validators.required)
            }
        )
    };



    save () {
        if (this.formGroup.valid) {
            const data=this.formGroup.value;
            return swal({
                title: 'New Employer',
                html: `<h4>You have requested pay an amount of Ghs${data.amount} as payment for loan borrowed by employer with id number ${data.employer}, Please confirm?</h4>`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, continue!'
            }).then((val) => {
                if (val) {
                    this.store.dispatch(
                        new AdminEmployerPayLoan(data)
                    ).subscribe(()=>this.formGroup.reset());
                }
            }).catch(err=>null);
        }
    }




}
