import { Component, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { CustomersModel } from 'app/models/customers/customers.model';
import { AppState } from 'app/states/app/state';
import { GetEmployerCustomers, EmployerUpdateCustomer, AdminGetEmployers } from 'app/states/employer/employer.actions';
import * as shortid from "shortid";
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
import { UserState } from 'app/states/users/users.state';
import { GetUsers, UpdateUser, AdminUpdateUser, AdminBlockCustomer } from 'app/states/users/users.actions';
import * as moment from "moment";

declare const swal:any;
declare const $: any;



@Component({
    selector: 'admin-customer-list',
    templateUrl: './admin-customer-list.component.html',
    styleUrls: ['./admin-customer-list.component.scss']
})
export class AdminCustomerListComponent implements OnInit {

    filter: CustomersModel[] = [];
    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    @Select(UserState.users) customers$: Observable<CustomersModel[]>;
    @Select(EmployerState.employers) employers$:Observable<any[]>;

    @ViewChild('dataTable', null) table;
    dataTable: any;

    p: number = 1;
    selected: CustomersModel;
    code: string;
    onsearch: boolean = false;
    selectedEmployer:string=null;



    constructor(private store: Store) {};


    ngOnInit(): void {
        this.store.dispatch(
            [
                new GetUsers(),
                new AdminGetEmployers()
            ]
        );
    };

    save () {
        const {_id}=this.selected;
        const data = Object.assign({},this.selected);
        delete data._id;
        this.store.dispatch(
            new AdminUpdateUser(
                {
                    id:_id,
                    data:data
                }
            )
        )
    }

    assignEmployer=()=>{
        if (!this.selectedEmployer) return swal(
            {
                html:`<h4>
                    Please select an employer you want to assign to customer.
                </h4>`
            }
        );
        const {firstname,_id}=this.selected;
        const employer=this.store.selectSnapshot(EmployerState.employers).find(employer=>employer._id===this.selectedEmployer);
        if (employer && employer._id){
            return swal({
                title: 'Assign Employer',
                html: `<h4>You have requested to assign <b>${employer.name}</b> to customer by name <b>${firstname}</b>, Please confirm your request.</h4>`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, continue!'
            }).then((result) => {
                if (result) {
                    return this.store.dispatch(
                        new UpdateUser(
                            {
                                id:_id,
                                data:{
                                    empId:employer._id,
                                    empcode:employer.empcode
                                }
                            }
                        )
                    )
                }
            })
        }
    }


    onSelected (customer) {
        this.selected=null;
        this.selected={...customer,dob:moment(new Date(customer.dob)).format('YYYY-MM-DD')};
        this.selectedEmployer=this.selected.getEmployer();
        
    }

    

    block (customer:CustomersModel) {
        this.store.dispatch(
            new AdminBlockCustomer(
                {
                    id:customer._id,
                    blocked:!customer.blocked
                }
            )
        )
    }


    onSearch (e) {
        try {
            const text: string = e.target.value;
            if (text.length > 0) {
                this.onsearch = true;
                const customers = this.store.selectSnapshot(UserState.users);
                this.filter = [];
                customers.forEach((customer:CustomersModel,i) => {
                    const name=customer.getName().toLowerCase();
                    const employer=customer.getEmployer('name').toLowerCase();
                    const phone=customer.mobile;
                    if (name.indexOf(text.toLowerCase()) > -1 || employer.indexOf(text.toLowerCase()) > -1 || phone.indexOf(text.toLowerCase())>-1) {
                        this.filter.push(customer);
                    }
                });
            } else this.onsearch = false;
        } catch (err) {
            this.onsearch = false;
        }
    }




}
