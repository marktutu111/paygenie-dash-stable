import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as shortid from "shortid";
import { Store, Select } from '@ngxs/store';
import { EmployerNewCustomer, AdminGetEmployers } from 'app/states/employer/employer.actions';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { AppState } from 'app/states/app/state';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';

@Component({
    selector: 'app-admin-add-customer',
    templateUrl: './admin-add-customer.component.html',
    styleUrls: ['./admin-add-customer.component.scss']
})
export class AdminAddCustomerComponent implements OnInit {


    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.employers) employers$:Observable<any[]>;
    formGroup: FormGroup;

    constructor(private store: Store) {};

    
    ngOnInit(): void {
        this.formGroup = new FormGroup(
            {
                empId: new FormControl(null,Validators.required),
                firstname: new FormControl(null, Validators.required),
                lastname: new FormControl(null, Validators.required),
                gender: new FormControl(null, Validators.required),
                mobile: new FormControl(null, Validators.required),
                email: new FormControl(null, Validators.required),
                dob: new FormControl(null, Validators.required),
                maxnet: new FormControl(null, Validators.required),
                approved: new FormControl(true,Validators.required),
                control: new FormControl('employer',Validators.required)
            }
        )

        this.store.dispatch(new AdminGetEmployers());

    };



    save () {
        if (this.formGroup.valid) {
            this.store.dispatch(
                new EmployerNewCustomer(this.formGroup.value)
            ).subscribe(()=>this.formGroup.patchValue(
                {
                    empId:null,
                    firstname:null,
                    lastname:null,
                    gender:null,
                    mobile:null,
                    email:null,
                    dob:null,
                    maxnet:null,
                    approved:true,
                    control:'employer'
                }
            ));
        }
    }




}
