import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'app/states/users/users.state';
import { Observable } from 'rxjs';
import { LoanModel } from 'app/models/loans/loans.model';
import { PayLoan } from 'app/states/users/users.actions';
import { AppState } from 'app/states/app/state';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import banks from 'app/models/banks';
import { LoansState } from 'app/states/loans/loans.state';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
import { GetLoans, GetLoanTransactions } from 'app/states/loans/loans.actions';
declare const swal: any;



@Component({
    selector: 'app-customer-loans',
    templateUrl: './customer-loans.component.html',
    styleUrls: ['./customer-loans.component.scss']
})
export class CustomerLoansComponent implements OnInit {

    @Select(LoansState.loans) loans$: Observable<LoanModel[]>;
    @Select(LoansState.transactions) transactions$:Observable<any[]>;
    @Select(ProgressBarState.loading) loading$: Observable<boolean>;

    onsearch: boolean = false;
    filter: LoanModel[] = [];
    selected: LoanModel;
    openTransactions:boolean=false;

    p:number = 1;
    formGroup: FormGroup;
    banks: any[] = banks;

    private user: any;

    constructor(private store: Store) {};

    
    ngOnInit(): void {
        try {

            this.user = this.store.selectSnapshot(AppState.user);
            this.formGroup = new FormGroup(
                {
                    account_number: new FormControl(null,Validators.required),
                    account_issuer: new FormControl(null,Validators.required),
                    account_type: new FormControl('momo',Validators.required),
                    amount: new FormControl(1,Validators.required)
                }
            )
            this.store.dispatch(new GetLoans());

        } catch (err) {

        }
    };


    openDetails (loan) {
        this.selected=loan;
    }


    loadTransactions(loan){
        this.openTransactions=true;
        return this.store.dispatch(
            new GetLoanTransactions(
                loan._id
            )
        )
    }


    onSearch (e) {
        try {
            const text: string = e.target.value;
            if (text.length > 0) {
                this.onsearch = true;
                const loans = this.store.selectSnapshot(LoansState.loans);
                this.filter = [];
                loans.forEach((loan:LoanModel) => {
                    const name=loan.getName().toLowerCase();
                    const employer=loan.getEmployer('name').toLowerCase();
                    if(name.indexOf(text.toLowerCase()) > -1 || employer.indexOf(text.toLowerCase()) > -1){
                        this.filter.push(loan);
                    }
                });
            } else this.onsearch = false;
        } catch (err) {
            this.onsearch = false;
        }
    }



    payLoan = () => {
        if (this.formGroup.valid) {
            const {amount}=this.formGroup.value;
            const {_id,getEmployer}=this.user;
            const data = Object.assign(this.formGroup.value, {
                employer:getEmployer(),
                customer:_id,
                amount:amount.toString()
            })
            return this.store.dispatch(
                new PayLoan(data)
            ).subscribe(() => this.formGroup.reset());
        }
    }


}
