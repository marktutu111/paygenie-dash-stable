import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { GetLoanSettings, SetSettings } from 'app/states/loans/loans.actions';
import { LoansState } from 'app/states/loans/loans.state';
import { Observable } from 'rxjs';
import { EmployerState } from 'app/states/employer/employer.state';
import { AdminGetEmployers, AdminUpdateEmployer } from 'app/states/employer/employer.actions';
import { AppState } from 'app/states/app/state';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit,OnDestroy {

    @Select(EmployerState.employers) employers$: Observable<any[]>;
    @Select(LoansState.settings) settings$:Observable<any[]>;

    formGroup:FormGroup;
    onEdit:boolean=false;
    private employerId:string=null;


    constructor(private store:Store, private fb:FormBuilder) {};

    ngOnInit(): void {
        this.formGroup=this.fb.group(
            {
                settings:new FormGroup(
                    {
                        type: new FormControl('employer',Validators.required),
                        rate: new FormControl(0,Validators.required),
                        penalty: new FormControl(0,Validators.required),
                        duration: new FormControl(0,Validators.required),
                        duration_type: new FormControl('months',Validators.required),
                        borrow_limit:new FormControl(30,Validators.required),
                        employer:new FormControl(null)
                    }
                ),
                salaries:new FormGroup(
                    {
                        salary_payment_date:new FormControl(null,Validators.required),
                        salary_payment_day:new FormControl(null,Validators.required)
                    }
                )
            }
        )

        this.store.dispatch(
            [
                new GetLoanSettings(),
                new AdminGetEmployers()
            ]
        )

    };

    save=()=>{
        if(!this.formGroup.valid)return;
        const settings=this.formGroup.get('settings');
        if(!settings.value.employer ||  settings.value.employer=== ''){
            return;
        }
        this.store.dispatch(
            [
                new SetSettings(settings.value),
                new AdminUpdateEmployer(
                    {
                        id:this.employerId || settings.value.employer,
                        data:this.formGroup.get('salaries').value
                    }
                )
            ]
        )
    };

    clear(){
        this.employerId=null;
        this.formGroup.patchValue(
            {
                settings:{
                    type:'employer',
                    rate:null,
                    penalty:null,
                    duration:null,
                    duration_type:null,
                    employer:null
                },
                salaries:{
                    salary_payment_date:null,
                    salary_payment_day:null
                }
            }
        )
    }



    edit(setting){
        this.employerId=setting.getEmployerId();
        this.formGroup.patchValue(
            {
                settings:{
                    ...setting,
                    employer:this.employerId
                },
                salaries:{
                    salary_payment_date:setting.getEmployer('salary_payment_date'),
                    salary_payment_day:setting.getEmployer('salary_payment_day')
                }
            }
        )
        this.onEdit=true;
        
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
    }


}
