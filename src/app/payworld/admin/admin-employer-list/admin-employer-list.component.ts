import { Component, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { CustomersModel } from 'app/models/customers/customers.model';
import { AppState } from 'app/states/app/state';
import { GetEmployerCustomers, EmployerUpdateCustomer, AdminGetEmployers, AdminUpdateEmployer, ApproveEmployer } from 'app/states/employer/employer.actions';
import * as shortid from "shortid";
import swal from 'sweetalert2';
import { EmployerModel } from 'app/models/employer/employer.model';


declare const $: any;



@Component({
    selector: 'admin-employer-list',
    templateUrl: './admin-employer-list.component.html',
    styleUrls: ['./admin-employer-list.component.scss']
})
export class AdminEmployerListComponent implements OnInit {

    filter:EmployerModel[] = [];
    @Select(EmployerState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.employers) employers$: Observable<any[]>;

    @ViewChild('dataTable', null) table;
    dataTable: any;

    toggleDate:boolean=false;
    p: number=1;
    selected:EmployerModel;
    code:string;
    onsearch:boolean = false;

    formGroup

    constructor(private store: Store) {};


    ngOnInit(): void {
        this.store.dispatch(new AdminGetEmployers());
    };


    save () {
        const {_id}=this.selected;
        const data =this.selected.getData();
        this.store.dispatch(
            new AdminUpdateEmployer(
                {
                    id:_id,
                    data:data
                }
            )
        )
    }

    approve(){
        const {_id}=this.selected;
        const data={
            id:_id,
            mobile:this.selected['mobile'],
            email:this.selected['email'],
            address:this.selected['address'],
            location:this.selected['location'],
            digital_address:this.selected['digital_address']
        }
        let missing=null;
        Object.keys(data).forEach(key=>{
            if(!data[key] || data[key]===''){
                missing=key;
            }
        });
        if(missing){
            return swal(
                {
                    html:`<h4>Please provide value ${missing}</h4>`
                }
            )
        }
        return this.store.dispatch(
            new ApproveEmployer(data)
        )

    }

    onSelected (employer) {
        this.selected=null;
        this.selected=employer;
    }

    

    block (employer:any) {
        this.store.dispatch(
            new AdminUpdateEmployer(
                {
                    id:employer._id,
                    data:{
                        blocked:!employer.blocked
                    }
                }
            )
        )
    }


    onSearch (e) {
        try {
            const text: string = e.target.value;
            if (text.length>0) {
                this.onsearch=true;
                const customers = this.store.selectSnapshot(EmployerState.employers);
                this.filter = [];
                customers.forEach((employer:EmployerModel,i)=>{
                    const name=employer.name.toLowerCase();
                    const empcode=employer.empcode.toLowerCase();
                    if (name.indexOf(text.toLowerCase()) > -1 || empcode.indexOf(text.toLowerCase())>-1) {
                        this.filter.push(employer);
                    }
                });
            } else this.onsearch=false;
        } catch (err) {
            this.onsearch=false;
        }
    }




}
