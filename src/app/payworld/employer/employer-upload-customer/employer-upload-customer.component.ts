import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AppState } from 'app/states/app/state';
import { EmployerService } from 'app/services/employer/employer.service';
import { UploaderService } from 'app/services/uploader/uploader';
import { Observable, Subscription } from 'rxjs';
import { Toggle } from 'app/components/progress-bar/progress-bar.actions';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';


declare const swal: any;
declare const $: any;


class CustomerModel {
    firstname;
    lastname;
    gender;
    mobile;
    email;
    maxnet;
    dob;
    empId;
    empcode;
    status:'failed'|'saved'|'pending'='pending';
    loading:boolean=false;
    approved:boolean=true;

    constructor (data) {
        this.firstname=data.firstname;
        this.lastname=data.lastname;
        this.gender=data.gender;
        this.mobile=data.mobile;
        this.email=data.email;
        this.maxnet=data.maxnet;
        this.dob=data.dob;
        this.empId=data.empId;
        this.empcode=data.empcode;
    }

    setStatus (status) {
        this.status=status;
    }

    setLoading (loading){
        this.loading=loading;
    }

    getData () {
        return {
            firstname:this.firstname,
            lastname:this.lastname,
            gender:this.gender,
            mobile:this.mobile,
            email:this.email,
            maxnet:this.maxnet,
            dob:this.dob,
            empId:this.empId,
            empcode:this.empcode,
            approved:this.approved,
            control:'employer'
        }
    }

    
}



@Component({
    selector: 'app-employer-upload-customer',
    templateUrl: './employer-upload-customer.component.html',
    styleUrls: ['./employer-upload-customer.component.scss']
})
export class EmployerUploadCustomerComponent implements OnInit {

    @Select(ProgressBarState.loading) loading$:Observable<boolean>;

    customers: any[] = [];

    onsearch: boolean = false;
    filter: any[] = [];
    index:number = 0;

    private subscription$:Subscription;


    constructor(private store: Store,private service:EmployerService,public uploader:UploaderService) {};


    ngOnInit(): void {
        this.subscription$=this.uploader.onCSV$.subscribe(v=>{
            const {_id,empcode}=this.store.selectSnapshot(AppState.user);
            this.customers=v.map(customer=>new CustomerModel(
                {
                    ...customer,
                    empId:_id,
                    empcode:empcode
                }
            ))
        });
    };


    send () {
        if (this.customers.length>0) {
            this.store.dispatch(new Toggle(true));
            let errors:boolean=false;
            this.customers.forEach(async(customer:CustomerModel,i:number) => {
                try {
                    customer.setLoading(true);
                    const data=customer.getData();
                    const response = await this.service.addCustomer(data);
                    customer.setLoading(false);
                    switch (response.success) {
                        case true:
                            customer.setStatus('saved');
                            break;
                        default:
                            customer.setStatus('failed');
                            errors=true;
                            break;
                    }

                    if (this.customers.length === i+1) {
                        this.store.dispatch(new Toggle(false));
                        let message:string='';
                        switch (errors) {
                            case true:
                                message=`Some customers could not be uploaded, Please check the table for errors.`
                                break;
                            default:
                                message=`All customers uploaded successfully.`;
                                break;
                        }
                        
                        return swal({html:message});
                    }
                } catch (err) {
                    errors=true;
                    customer.setStatus('failed');
                    customer.setLoading(false);
                    if (this.customers.length === i+1) {
                        this.store.dispatch(new Toggle(false));
                        swal({html: `<h4>Customers were uploaded with errors ${err.message}</h4>`})
                    }
                }
            });
        }
    }



    onSearch (e) {
        try {

            const text: string = e.target.value;
            if (text.length > 0) {
                this.onsearch = true;
                this.filter = [];
                this.customers.forEach(customer => {
                    Object.keys(customer).forEach(key => {
                        if (customer[key] && customer[key].toLowerCase().indexOf(text.toLowerCase()) > -1) {
                            this.filter.push(customer);
                        }
                    })
                });

            } else {
                this.onsearch = false;
            }
        } catch (err) {
            this.onsearch = false;
        }
    }


    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.unsubscribe();
    }



}
