import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { EmployerLoanAggregator, FetchEmployer } from 'app/states/employer/employer.actions';

import * as shortid from "shortid";
import { AppState } from 'app/states/app/state';
import { GetEmployerLoanSummary } from 'app/states/loans/loans.actions';
import { LoansState } from 'app/states/loans/loans.state';



declare const $: any;

@Component({
  selector: 'app-employer-dashboard',
  templateUrl: './employer-dashboard.component.html',
  styleUrls: ['./employer-dashboard.component.scss']
})
export class EmployerDashboardComponent implements OnInit {


    @Select(LoansState.summary) summary$: Observable<any>;
    constructor (private store: Store) {};

    public ngOnInit() {
      const _id = this.store.selectSnapshot(AppState.user)._id;
      this.store.dispatch(
        new GetEmployerLoanSummary(_id)
      )
    }




}
