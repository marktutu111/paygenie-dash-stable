import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { GetEmployerLoanApplicants, FilterByDate, FilterByPaymentsDue } from 'app/states/employer/employer.actions';
import { AppState } from 'app/states/app/state';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from "moment";
import { LoanModel } from 'app/models/loans/loans.model';



@Component({
    selector: 'app-employer-loans',
    templateUrl: './employer-loans.component.html',
    styleUrls: ['./employer-loans.component.scss']
})
export class EmployerLoansComponent implements OnInit {

    filter: any[] = [];
    onsearch: boolean = false;

    @Select(EmployerState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.loans) loans$: Observable<any[]>;

    selected: any;
    p: number = 1;
    formGroup: FormGroup;
    private empId: string;
    private interval$:any;


    constructor(private store: Store) {};



    ngOnInit(): void {
        const today = moment().format('YYYY-MM-DD').toString();
        this.empId = this.store.selectSnapshot(AppState.user)._id;
        this.formGroup = new FormGroup(
            {
                from: new FormControl(today, Validators.required),
                to: new FormControl(today, Validators.required),
            }
        )

        this.store.dispatch(
            new GetEmployerLoanApplicants(this.empId)
        )

        this.interval$=setInterval(()=>{
            this.store.dispatch(
                this.store.dispatch(
                    new GetEmployerLoanApplicants(this.empId,false)
                )
            );
        },1000*3);


    };



    openDetails (loan) {
        this.selected = loan;
    }


    search () {
        if (this.formGroup.valid) {
            this.store.dispatch(
                new FilterByDate(this.formGroup.value)
            )

        }
    }


    due () {
        this.store.dispatch(
            new FilterByPaymentsDue(this.empId)
        )
    }



    onSearch (e) {
        try {

            const text: string = e.target.value;

            if (text.length > 0) {
                this.onsearch=true;
                const loans = this.store.selectSnapshot(EmployerState.loans);
                this.filter = [];
                loans.forEach((loan:LoanModel) => {
                    const name=loan.getName().toLowerCase();
                    const employer=loan.getEmployer('name').toLowerCase();
                    if (name.toLowerCase().indexOf(text.toLowerCase()) > -1 || employer.toLowerCase().indexOf(text.toLowerCase()) > -1) {
                        this.filter.push(loan);
                    }
                });
            } else {
                this.onsearch = false;
            }
        } catch (err) {
            this.onsearch = false;
        }
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        clearInterval(this.interval$);
    }
    



}
