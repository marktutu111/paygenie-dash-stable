import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EmployerComponent } from './employer.component';
import { EmployerCustomerListComponent } from './employer-customer-list/employer-customer-list.component';
import { EmployerAddCustomerComponent } from './employer-add-customer/employer-add-customer.component';
import { EmployerUploadCustomerComponent } from './employer-upload-customer/employer-upload-customer.component';
import { EmployerLoansComponent } from './employer-loans/employer-loans.component';
import { EmployerDashboardComponent } from './employer-dashboard/employer-dashboard.component';
import { MdModule } from 'app/md/md.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/app.module';
import { MatNativeDateModule } from '@angular/material';
import { SidebarModule } from 'app/sidebar/sidebar.module';
import { NavbarModule } from 'app/shared/navbar/navbar.module';
import { FooterModule } from 'app/shared/footer/footer.module';
import { FixedpluginModule } from 'app/shared/fixedplugin/fixedplugin.module';

import { NgxPaginationModule } from "ngx-pagination";
import { ProgressBarModule } from 'app/components/progress-bar/progress-bar.module';
import { UploaderService } from 'app/services/uploader/uploader';
import { EmployerInvoiceListComponent } from './employer-invoice-list/employer-invoice-list.component';
import { EmployerResetPasswordComponent } from './employer-reset-password/employer-reset-password.component';
import { EmployerChangePinGuard } from 'app/guards/emp-changepin-guard';
import { SessionService } from 'app/services/sessions/sessions.service';




const routes: Routes = [
    {
        path: '',
        component: EmployerComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                canActivate:[
                    EmployerChangePinGuard
                ]
            },
            {
                path: 'dashboard',
                component: EmployerDashboardComponent,
                canActivate:[
                    EmployerChangePinGuard
                ]
            },
            {
                path: 'list',
                component: EmployerCustomerListComponent,
                canActivate:[
                    EmployerChangePinGuard
                ]
            },
            {
                path: 'new',
                component: EmployerAddCustomerComponent,
                canActivate:[
                    EmployerChangePinGuard
                ]
            },
            {
                path: 'upload',
                component: EmployerUploadCustomerComponent,
                canActivate:[
                    EmployerChangePinGuard
                ]
            },
            {
                path: 'loans',
                component: EmployerLoansComponent,
                canActivate:[
                    EmployerChangePinGuard
                ]
            },
            {
                path:'invoice',
                component:EmployerInvoiceListComponent,
                canActivate:[
                    EmployerChangePinGuard
                ]
            },
            {
                path:'cpass',
                component:EmployerResetPasswordComponent
            }
        ]
    }
]





@NgModule({
    declarations: [
        EmployerComponent,
        EmployerCustomerListComponent,
        EmployerAddCustomerComponent,
        EmployerUploadCustomerComponent,
        EmployerLoansComponent,
        EmployerDashboardComponent,
        EmployerInvoiceListComponent,
        EmployerResetPasswordComponent
    ],
    imports: [ 
        CommonModule,
        MdModule,
        FormsModule,
        MaterialModule,
        MatNativeDateModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedpluginModule,
        NgxPaginationModule,
        ReactiveFormsModule,
        ProgressBarModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [UploaderService,EmployerChangePinGuard,SessionService],
})
export class EmployerModule {}