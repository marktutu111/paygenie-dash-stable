import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as shortid from "shortid";
import { Store, Select } from '@ngxs/store';
import { EmployerNewCustomer } from 'app/states/employer/employer.actions';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { AppState } from 'app/states/app/state';

@Component({
    selector: 'app-employer-add-customer',
    templateUrl: './employer-add-customer.component.html',
    styleUrls: ['./employer-add-customer.component.scss']
})
export class EmployerAddCustomerComponent implements OnInit {


    @Select(EmployerState.loading) loading$: Observable<boolean>;
    formGroup: FormGroup;

    constructor(private store: Store) {};

    
    ngOnInit(): void {
        const {_id,empcode}=this.store.selectSnapshot(AppState.user);
        this.formGroup = new FormGroup(
            {
                empcode: new FormControl(empcode,Validators.required),
                empId: new FormControl(_id,Validators.required),
                firstname: new FormControl(null, Validators.required),
                lastname: new FormControl(null, Validators.required),
                gender: new FormControl(null, Validators.required),
                mobile: new FormControl(null, Validators.required),
                email: new FormControl(null, Validators.required),
                dob: new FormControl(null, Validators.required),
                maxnet: new FormControl(null, Validators.required),
                approved: new FormControl(true,Validators.required),
                control:new FormControl('employer',Validators.required)
            }
        )
    };


    save () {
        if (this.formGroup.valid) {
            const data = this.formGroup.value;
            this.store.dispatch(
                new EmployerNewCustomer(data)
            ).subscribe(() =>this.formGroup.patchValue(
                {
                    empcode:null,
                    empId:null,
                    firstname:null,
                    lastname:null,
                    gender:null,
                    mobile:null,
                    email:null,
                    dob:null,
                    maxnet:null,
                    approved:null,
                    control:'employer'
                }
            ));

        }
    }




}
