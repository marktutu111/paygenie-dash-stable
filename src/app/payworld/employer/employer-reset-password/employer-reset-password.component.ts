import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
import { AppState } from 'app/states/app/state';
import { EmployerChangePassword } from 'app/states/employer/employer.actions';
import { Observable } from 'rxjs';
declare const swal:any;

@Component({
    selector: 'app-employer-reset-password',
    templateUrl: './employer-reset-password.component.html',
    styleUrls: ['./employer-reset-password.component.scss']
})
export class EmployerResetPasswordComponent implements OnInit {

    @Select(ProgressBarState.loading) loading$:Observable<boolean>;

    formGroup:FormGroup;
    constructor(private store:Store) {};


    ngOnInit(): void {
        this.formGroup=new FormGroup(
            {
                oldpin:new FormControl(null,Validators.required),
                newpin:new FormControl(null,Validators.required),
                confirmpin:new FormControl(null,Validators.required)
            }
        )
    };

    save(){
        if(!this.formGroup.valid)return;
        const data=this.formGroup.value;
        if(data.confirmpin !== data.newpin){
            return swal({
                html: `<h4>New PIN does not match</h4>`
            }).catch(err => null);
        }
        const employer=this.store.selectSnapshot(AppState.user)._id;
        return this.store.dispatch(
            new EmployerChangePassword(
                {
                    oldpin:data.oldpin,
                    newpin:data.newpin,
                    id:employer
                }
            )
        )
    }


}
