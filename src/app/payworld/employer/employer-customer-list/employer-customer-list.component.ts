import { Component, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { CustomersModel } from 'app/models/customers/customers.model';
import { AppState } from 'app/states/app/state';
import { GetEmployerCustomers, EmployerUpdateCustomer, ApproveCustomer, EmployerApproveAll } from 'app/states/employer/employer.actions';
import * as shortid from "shortid";
import swal from "sweetalert2";

declare const $: any;



@Component({
    selector: 'app-employer-customer-list',
    templateUrl: './employer-customer-list.component.html',
    styleUrls: ['./employer-customer-list.component.scss']
})
export class EmployerCustomerListComponent implements OnInit {

    filter: CustomersModel[] = [];
    @Select(EmployerState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.customers) customers$: Observable<CustomersModel[]>;

    @ViewChild('dataTable', null) table;
    dataTable: any;

    p: number = 1;
    selected: CustomersModel;
    code: string;
    onsearch: boolean = false;
    private $timeout:any;

    formGroup

    constructor(private store: Store) {};


    ngOnInit(): void {
        try {

            this.code = this.store.selectSnapshot(AppState.user)._id;
            this.store.dispatch(new GetEmployerCustomers(this.code));
            this.$timeout=setInterval(()=>{
                this.store.dispatch(
                    new GetEmployerCustomers(this.code,false)
                );
            },1000*3)
        } catch (err) {
            
        }

    };


    save () {
        const {_id}=this.selected;
        const data = Object.assign({},this.selected);
        delete data._id;
        this.store.dispatch(
            new EmployerUpdateCustomer(
                {
                    id:_id,
                    data:data
                }
            )
        )
    }

    approve(type:'APPROVE'|'UNAPPROVE'|'ALL',customer){
        let approved:boolean=false;
        let message:string='';
        switch (type) {
            case 'APPROVE':
                message='<h4>You have requested to <b>Approve</b> this customer, Please confirm your request?</h4>'
                approved=true;
                break;
            case 'ALL':
                message='<h4>You have requested to Approve all customers, Please confirm your request?</h4>'
                break;
            default:
                approved=false;
                message='<h4>You have requested to <b>Block</b> this customer, Please confirm your request?</h4>'
                break;
        }
        swal(
            {
                html:message,
                cancelButtonColor:'red',
                cancelButtonText:'No, Cancel',
                showCancelButton:true,
                confirmButtonText:'Yes, Continue'
            }
        ).then(value=>{
            if(value){
                if(type==='ALL'){
                    this.store.dispatch(
                        new EmployerApproveAll(
                            {
                                id:this.code
                            }
                        )
                    )
                }else{
                    this.store.dispatch(
                        new ApproveCustomer(
                            {
                                id:customer._id,
                                approved:approved
                            }
                        )
                    )
                }
            }
        }).catch(err=>null);
    }

    onSelected (customer) {
        this.selected=null;
        this.selected=customer;
    }

    block (customer:CustomersModel) {
        this.store.dispatch(
            new EmployerUpdateCustomer(
                {
                    id:customer._id,
                    data:{
                        blocked:!customer.blocked
                    }
                }
            )
        )
    }

    onAction(e,customer:CustomersModel){
        const type=e.target.value;
        switch (type) {
            case 'unapprove':
                this.approve('UNAPPROVE',customer);
                break;
            case 'approve':
                this.approve('APPROVE',customer);
                break;
            default:
                break;
        }
    }

    onSearch (e) {
        try {
            const text:string=e.target.value;
            if (text.length > 0) {
                this.onsearch=true;
                const customers = this.store.selectSnapshot(EmployerState.customers);
                this.filter = [];
                customers.forEach((customer:CustomersModel,i)=>{
                    const name=customer.getName().toLowerCase();
                    const mobile=customer.mobile;
                    const employer=customer.getEmployer('name');
                    if (
                        name.indexOf(text.toLowerCase()) > -1 || 
                        employer.indexOf(text.toLowerCase())>-1 || 
                        mobile.indexOf(text.toLowerCase())>-1
                    ){
                        this.filter.push(customer);
                    }
                });
            } else this.onsearch = false;
        } catch (err) {
            this.onsearch = false;
        }
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        clearInterval(this.$timeout);
    }



}
