import { Component, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';
import { CustomersModel } from 'app/models/customers/customers.model';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
import { AdminGetInvoice } from 'app/states/employer/employer.actions';
import { LoanModel, LoansInterfaceModel } from 'app/models/loans/loans.model';
import { AppState } from 'app/states/app/state';


declare const $: any;



@Component({
    selector: 'employer-invoice-list',
    templateUrl: './employer-invoice-list.component.html',
    styleUrls: ['./employer-invoice-list.component.scss']
})
export class EmployerInvoiceListComponent implements OnInit {

    filter: CustomersModel[] = [];
    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    @Select(EmployerState.invoices) invoices$: Observable<any[]>;
    

    @ViewChild('dataTable', null) table;
    dataTable: any;

    p: number = 1;
    loans:LoanModel[]=[];
    selectedId:string=null;
    code:string;
    onsearch:boolean = false;

    constructor(private store: Store) {};


    ngOnInit(): void {
        const id:string=this.store.selectSnapshot(AppState.user)._id;
        this.store.dispatch(new AdminGetInvoice(id));
    };


    onSearch (e) {
        try {
            const text: string = e.target.value;
            if (text.length > 0) {
                this.onsearch = true;
                const invoices = this.store.selectSnapshot(EmployerState.invoices);
                this.filter = [];
                invoices.forEach((invoice,i) => {
                    const invoceId=invoice.invoiceId.toLowerCase();
                    const empcode=invoice.employer.empcode.toLowerCase();
                    const name=invoice.employer.name.toLowerCase();
                    if (invoceId.indexOf(text.toLowerCase()) > -1 || name.indexOf(text.toLowerCase()) > -1 || empcode.indexOf(text.toLowerCase()) > -1) {
                        this.filter.push(invoice);
                    }
                });
            } else this.onsearch = false;
        } catch (err) {
            this.onsearch = false;
        }
    }


    onSelected(invoice){
        this.loans=invoice.loans.map(loan=>new LoanModel(loan));
        this.selectedId=invoice._id;
    }




}
