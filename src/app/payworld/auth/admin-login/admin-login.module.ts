import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminLoginComponent } from './admin-login.component';
import { ReactiveFormsModule } from '@angular/forms';



const routes: Routes = [
    {
        path: '',
        component: AdminLoginComponent
    }
]




@NgModule({
    declarations: [
        AdminLoginComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class AdminLoginModule {}