import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { EmployerAuthenticate, AdminAuthenticate } from 'app/states/employer/employer.actions';
import { EmployerState } from 'app/states/employer/employer.state';
import { Observable } from 'rxjs';

declare var $: any;

@Component({
    selector: 'admin-login',
    templateUrl: './admin-login.component.html'
})

export class AdminLoginComponent implements OnInit {


    formGroup: FormGroup;
    @Select(EmployerState.loading) loading$: Observable<boolean>;


    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;


    constructor(private element: ElementRef, private store: Store) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }


    ngOnInit() {
        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

        setTimeout(() => {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);

        // Handle Authentication here
        this.formGroup = new FormGroup(
            {
                username: new FormControl(null, Validators.required),
                password: new FormControl(null, Validators.required)
            }
        )

    }


    login () {
        if (this.formGroup.valid) {
            this.store.dispatch(
                new AdminAuthenticate(this.formGroup.value)
            )
        }
    }



}
