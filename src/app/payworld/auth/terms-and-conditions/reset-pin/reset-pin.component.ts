import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'app/states/users/users.state';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import * as shortid from "shortid";
import { ResetPassword } from 'app/states/users/users.actions';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';



@Component({
    selector: 'app-reset-pin',
    templateUrl: './reset-pin.component.html',
    styleUrls: ['./reset-pin.component.scss']
})
export class ResetPinComponent implements OnInit {


    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    formGroup: FormGroup;

    constructor(private store: Store) {};


    ngOnInit(): void {
        try {

            const { account,empId } = this.store.selectSnapshot(UserState.temp);
            this.formGroup = new FormGroup(
                {
                    account: new FormControl(account,Validators.required),
                    password: new FormControl(null,Validators.required),
                    empId:new FormControl(empId,Validators.required),
                    otp: new FormControl(null,Validators.required),
                    confirm: new FormControl(null,Validators.required)
                }
            )
        } catch (err) {
            
        }
    };



    send () {
        if (this.formGroup.valid) {
            const {
                password,
                confirm
            } = this.formGroup.value;
            if (password !== confirm) {
                return swal(
                    {
                        html: `<h4>Pin does not match</h4>`
                    }
                )
            }
            const data = Object.assign({},this.formGroup.value);
            delete data.confirm;
            return this.store.dispatch(
                new ResetPassword(data)
            )
        }
    }



}
