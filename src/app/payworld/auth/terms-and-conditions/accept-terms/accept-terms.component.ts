import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector: 'app-accept-terms',
    templateUrl: './accept-terms.component.html',
    styleUrls: ['./accept-terms.component.scss']
})
export class AcceptTermsComponent implements OnInit {


    formGroup: FormGroup;

    constructor(private router: Router) {};


    ngOnInit(): void {
        this.formGroup = new FormGroup(
            {
                agreed: new FormControl(null, Validators.required),
                usercode: new FormControl(null)
            }
        )
    };


    procede () {
        this.router.navigate(['./auth/change-pin'], { state: this.formGroup.value });
    }




}
