import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { UserLogin, ResetOTP } from 'app/states/users/users.actions';
import { Router } from '@angular/router';
import { AutoLogin } from 'app/states/app/actions';
import { Toggle } from 'app/components/progress-bar/progress-bar.actions';
import { UserService } from 'app/services/users/users.service';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';


declare const swal:any;
declare var $: any;

@Component({
    selector: 'app-user-login',
    templateUrl: './user-login.component.html',
    styleUrls: ['./user-login.component.scss']
})

export class UserLoginComponent implements OnInit {

    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    formGroup: FormGroup;
    employers:any[]=[];


    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;


    constructor(private element: ElementRef, private store:Store, private router:Router,private service:UserService) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }



    ngOnInit() {
        var navbar : HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

        setTimeout(() => {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);

        // Handle Authentication here
        this.formGroup = new FormGroup(
            {
                username: new FormControl(null, Validators.required),
                pswd: new FormControl(null, Validators.required),
                empId:new FormControl(null,Validators.required)
            }
        );

        // this.store.dispatch(
        //     new AutoLogin()
        // )

    }

    getLogins=async(phone?:string)=>{
        try {

            if(!phone){
                if(!this.formGroup.get('username').valid && !this.formGroup.get('pswd').valid)return;
                if(this.formGroup.get('username').value.length !== 10){
                    return;
                }
                if(this.formGroup.get('empId').valid){
                    return this.login();
                }
            }

            this.store.dispatch(new Toggle(true));
            const response=await this.service.getLogins({phone:phone || this.formGroup.get('username').value});
            
            if(!response.success){
                throw Error(response.message);
            }
            const data=response.data;
            if(data.length>1){
                let employers=[];
                data.forEach(d=>employers.push({ name:d['empId'].name,value:d['empId']._id }));
                this.employers=employers;
                if(!phone){
                    swal({html: `<h4>Dear customer!\nYou have multiple accounts please choose which one you want to login into.</h4>`});
                }else{
                    const data={};
                    this.employers.forEach(emp=>data[emp.value]=emp['name']);
                    swal(
                        {
                            html:`<h4>Dear customer!\nYou have multiple accounts please choose which one you want to reset.</h4>`,
                            input: 'select',
                            inputOptions:data,
                            inputPlaceholder:'choose account',
                            showCancelButton:true,
                        }
                    ).then(v=>{
                        if(v){
                            this.store.dispatch(
                                new ResetOTP(
                                    {
                                        account:phone,
                                        empId:v
                                    }
                                )
                            )
                        }
                    }).catch(err=>null);
                }
            } else {
                const empId=data['0']['empId']['_id'];
                if(!phone){
                    this.formGroup.patchValue(
                        {
                            empId:empId
                        }
                    )
                    this.login();
                }else {
                    this.store.dispatch(
                        new ResetOTP(
                            {
                                account:phone,
                                empId:empId
                            }
                        )
                    )
                }
            }
        } catch (err) {
            swal({html: `${err.message}`});
            this.store.dispatch(
                new Toggle(false)
            )
        }
    }

    login () {
        if (this.formGroup.valid) {
            const {
                username,
                pswd,
                empId
            } = this.formGroup.value;
            const data = {
                email: username,
                password: pswd,
                empId:empId
            }
            this.store.dispatch(
                new UserLogin(data)
            )
        }
    }



    reset () {
        try {
            swal(
                {
                    html: `<h4>Provide your phonenumber, We will send you an <b>OTP</b> to reset your account. <br/></h4>`,
                    input: 'text',
                    showCancelButton: true,
                    cancelButtonColor: 'red'
                }
            ).then(val => {
                if (typeof val === 'string' && val !== '' && val.length===10){
                    return this.getLogins(val);
                }
                swal(
                    {
                        html: `<h4>Invalid Phonenumber</h4>`
                    }
                )
            }).catch(err=>null);
        } catch (err) {
            
        }
    }



}
