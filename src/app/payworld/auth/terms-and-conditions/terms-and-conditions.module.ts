import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TermsAndConditionsComponent } from './terms-and-conditions.component';
import { AcceptTermsComponent } from './accept-terms/accept-terms.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ResetPinComponent } from './reset-pin/reset-pin.component';
import { ResetPinGuard } from 'app/guards/resetpin.guard';
import { UserLoginComponent } from './user-login/user-login.component';



const route: Routes = [
    {
        path: '',
        component: TermsAndConditionsComponent,
        children: [
            {
                path:'',
                redirectTo:'login'
            },
            {
                path: 'accept-terms',
                component: AcceptTermsComponent
            },
            {
                path: 'change-pin',
                component: ResetPinComponent,
                canActivate: [
                    ResetPinGuard
                ]
            },
            {
                path: 'login',
                component: UserLoginComponent
            }
        ]
    }
]


@NgModule({
    declarations: [
        TermsAndConditionsComponent,
        AcceptTermsComponent,
        ResetPinComponent,
        UserLoginComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(route)
    ],
    exports: [],
    providers: [],
})
export class TermsandConditionsModule {};