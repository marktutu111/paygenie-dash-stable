import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { EmployerLoginComponent } from './employer-login.component';
import { ReactiveFormsModule } from '@angular/forms';



const routes: Routes = [
    {
        path: '',
        component: EmployerLoginComponent
    }
]




@NgModule({
    declarations: [
        EmployerLoginComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class EmployerLoginModule {}