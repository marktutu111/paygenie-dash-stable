import { Component, OnInit, Input } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators, FormBuilder, NgControl } from '@angular/forms';
import { ProgressBarState } from 'app/components/progress-bar/progress-bar.state';
import { BillpayState } from 'app/states/billpay/billpay.state';
import { GetServices, GetServiceList, PayBill, GetCustomerBills } from 'app/states/billpay/billpay.actions';
import { ServiceModel, ServiceListModel } from 'app/models/services/services.model';
import { AppState } from 'app/states/app/state';

declare const swal: any;


@Component({
    selector: 'billpay',
    templateUrl: './billpay.component.html',
    styleUrls: ['./billpay.component.scss']
})
export class BillPayComponent implements OnInit {


    @Select(ProgressBarState.loading) loading$: Observable<boolean>;
    @Select(BillpayState.services) services$: Observable<any[]>;
    @Select(BillpayState.serviceList) serviceList$: Observable<any[]>;
    @Select(BillpayState.bills) bills$:Observable<any[]>;

    formGroup: FormGroup;
    selected: ServiceModel=null;
    selectedForms:any[]=[];
    serviceList:any[]=[];
    serviceListItem:ServiceListModel=null;
    serviceDetails:any={};

    p:number=1;


    constructor(private store: Store, private fb:FormBuilder) {};



    ngOnInit(): void {
        try {

            const {_id} =this.store.selectSnapshot(AppState.user);
            this.formGroup = this.fb.group(
                {
                    sid:new FormControl(null),
                    name:new FormControl(null),
                    a: new FormGroup(
                        {
                            msisdn:new FormControl(null),
                            data_bundle_type:new FormControl(null),
                            amount:new FormControl(null)
                        }
                    ),
                    b: new FormGroup(
                        {
                            customerNumber:new FormControl(null),
                            is_monthly:new FormControl('true'),
                            amount:new FormControl(null)
                        }
                    ),
                    account_number: new FormControl(null,Validators.required),
                    account_issuer: new FormControl(null,Validators.required),
                    account_type: new FormControl('momo',Validators.required)
                }
            )

            this.store.dispatch(
                [
                    new GetServices(),
                    new GetCustomerBills(_id)
                ]
            );
        } catch (err) {
            
        }
    };
    
    
    send () {
        try {
            const user=this.store.selectSnapshot(AppState.user);

            const {
                sid,
                account_issuer,
                account_number,
                name,
            }=this.formGroup.value;

            const type=this.selected.getType();
            const serviceDetails=this.formGroup.get(type);

            if (type==='b'){
                const {balance}:any =this.store.selectSnapshot(BillpayState.serviceList);
                serviceDetails.value['amount']=balance;
                serviceDetails.value['is_monthly']='true';
                this.formGroup.value['name']=this.selected.name;
            }

            if (!serviceDetails.valid) {
                return;
            }

            const data={
                customer:user._id,
                account_number:account_number,
                account_issuer:account_issuer,
                account_type:'momo',
                sid:sid,
                serviceDetails:serviceDetails.value,
                name: name,
            }

            let error:string=null;
            Object.keys(data).forEach(key=>{
                if (!key || !data[key] || data[key]==='') {
                    return error=key;
                }
            });

            if (typeof error === 'string') {
                return swal({html:`<h4>please provide value for ${error}</h4>`})
            }
            this.store.dispatch(new PayBill(data));
        } catch (err) {
            
        }
    };



    onChange=(e)=>{
        try {
            const id=e.target.value;
            this.serviceListItem=null;
            this.selectedForms=[];
            this.serviceList=[];
    
            const services: any[] = this.store.selectSnapshot(BillpayState.services);
            this.selected = services.find(service=>service._id===id);
            if (this.selected && this.selected._id){
                const {sid_buy,sid_list,getType}=this.selected;
                const type=getType();
                this.formGroup.patchValue({sid:sid_buy});
                this.formGroup.get(type).reset();
                switch (type) {
                    case 'a':
                        return this.store.dispatch(new GetServiceList(
                            {sid:sid_list}
                        ));
                    default:
                        break;
                }
            }
        } catch (err) {
            
        }
    }

    getDetails=()=>{
        const {getType,sid_list}=this.selected;
        const {customerNumber}= this.formGroup.get(getType()).value;
        this.store.dispatch(
            new GetServiceList(
                {
                    sid:sid_list,
                    serviceDetails:{
                        customerNumber:customerNumber
                    }
                }
            )
        )
    }


    setFormValues=()=>{
        this.formGroup.patchValue({name:this.serviceListItem.getDescription()});
        this.selected.getForms().forEach(form=>{
            this.formGroup.get(this.selected.getType()).patchValue(
                {
                    [form]:this.selected.geFormValues(this.serviceListItem,form)
                }
            )
        })
    }


    getList=(e)=>{
        const id:string=e.target.value;
        const lists:any=this.store.selectSnapshot(BillpayState.serviceList);
        const selected=lists.find(list=>list.planName===id);
        if(selected && selected.planName){
            this.serviceList=selected.content.map(sel=>new ServiceListModel(sel));
        }
    }

    getListItem=(e)=>{
        const id=e.target.value;
        this.serviceListItem=this.serviceList.find((i:ServiceListModel)=>i.productID===id);
        this.setFormValues();
    }


    



}
