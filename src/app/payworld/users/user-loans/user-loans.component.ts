import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'app/states/users/users.state';
import { Observable } from 'rxjs';
import { LoanModel } from 'app/models/loans/loans.model';
import { GetUserLoans, PayLoan, SendOTP } from 'app/states/users/users.actions';
import { AppState } from 'app/states/app/state';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import banks from 'app/models/banks';
declare const swal: any;



@Component({
    selector: 'app-user-loans',
    templateUrl: './user-loans.component.html',
    styleUrls: ['./user-loans.component.scss']
})
export class UserLoansComponent implements OnInit {

    @Select(UserState.loans) loans$: Observable<LoanModel[]>;
    @Select(UserState.loading) loading$: Observable<boolean>;

    onsearch: boolean = false;
    filter: LoanModel[] = [];
    selected: LoanModel=null;
    modalType:string='';

    p: number = 1;
    formGroup: FormGroup;
    banks: any[] = banks;

    private user: any;
    private interval$:any;

    constructor(private store: Store) {};

    
    ngOnInit(): void {
        try {

            this.user = this.store.selectSnapshot(AppState.user);
            this.formGroup = new FormGroup(
                {
                    account_number: new FormControl(null,Validators.required),
                    account_issuer: new FormControl(null,Validators.required),
                    account_type: new FormControl('momo',Validators.required),
                    amount: new FormControl(1,Validators.required)
                }
            )

            this.store.dispatch(new GetUserLoans(this.user._id));
            this.interval$=setInterval(()=>{
                this.store.dispatch(
                    new GetUserLoans(
                        this.user._id,
                        false
                    )
                );
            },1000*3);
        } catch (err) {

        }
    };


    openDetails (loan,type:string=null) {
        this.selected=loan;
        this.modalType=type;
        if(type==='pay'){
            this.formGroup.patchValue(
                {
                    amount:this.selected['amount_remaining']
                }
            )
        }
    }


    onSearch (e) {
        try {

            const text: string = e.target.value;
            if (text.length > 0) {
                this.onsearch = true;
                const loans = this.store.selectSnapshot(UserState.loans);
                this.filter = [];
                loans.forEach((loan: any) => {
                    Object.keys(loan).forEach(key => {
                        if (loan[key] && loan[key].toLowerCase().indexOf(text.toLowerCase()) > -1) {
                            this.filter.push(loan);
                        }
                    })
                });
            } else {
                this.onsearch = false;
            }
        } catch (err) {
            this.onsearch = false;
        }
    }



    payLoan = () => {
        if (this.formGroup.valid) {
            const {amount}=this.formGroup.value;
            const {_id,getEmployer}=this.user;
            const data = Object.assign(this.formGroup.value, {
                employer:getEmployer(),
                customer:_id,
                amount:amount.toString()
            })
            return this.store.dispatch(
                new PayLoan(data)
            ).subscribe(()=>{
                this.selected=null;
                this.formGroup.patchValue(
                    {
                        account_number:null,
                        account_issuer:null,
                        account_type:'momo',
                        amount:1
                    }
                )
            });
        }
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        clearInterval(this.interval$);
    }




}
