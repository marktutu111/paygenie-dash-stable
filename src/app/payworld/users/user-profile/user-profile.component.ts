import { Component, OnInit, OnDestroy } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AppState } from 'app/states/app/state';
import { Observable, Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UserState } from 'app/states/users/users.state';
import { UserLoanSummary, UpdateUser, ChangePin, UpdatePassword, GetProfile, GetUserSummary } from 'app/states/users/users.actions';
import * as shortid from "shortid";
import swal from 'sweetalert2';



@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {

    @Select(AppState.user) user$: Observable<any>;
    @Select(UserState.loading) loading$: Observable<boolean>;
    @Select(UserState.profile) profile$: Observable<any>;
    @Select(UserState.summary) summary$:Observable<any>;

    formGroup: FormGroup;
    private userSubscripton: Subscription;

    constructor(private store: Store, private fb: FormBuilder) {};



    ngOnInit(): void {
        try {
            
            const user = this.store.selectSnapshot(AppState.user);
            this.formGroup = this.fb.group(
                {
                    user: new FormGroup(
                        {
                            firstname: new FormControl(null, Validators.required),
                            lastname: new FormControl(null, Validators.required),
                            email: new FormControl(null, Validators.required),
                            gender: new FormControl('MALE', Validators.required),
                            mobile: new FormControl(null, Validators.required),
                            dob: new FormControl(null, Validators.required),
                            maxnet: new FormControl(null, Validators.required),
                            empcode: new FormControl(null)
                        }
                    ),
                    pin: new FormGroup(
                        {
                            id: new FormControl(user['_id'], Validators.required),
                            oldpassword: new FormControl(null, Validators.required),
                            newpassword: new FormControl(null, Validators.required),
                        }
                    )
                }
            )

            this.store.dispatch(
                [
                    new GetProfile(user._id),
                    new GetUserSummary(user._id)
                ]
            )

            this.userSubscripton = this.user$.subscribe(user => this.formGroup.patchValue({ user : user || {} }));

        } catch (err) {
            
        }

    };


    

    update () {
        if (this.formGroup.get('user').valid) {
            const {
                user
            } = this.formGroup.value;
            const id = this.store.selectSnapshot(AppState.user)._id;
            this.store.dispatch(
                new UpdateUser(
                    {
                        id: id,
                        data: user
                    }
                )
            )
        }
    }


    changePin () {
        const valid = this.formGroup.get('pin').valid;
        if (valid) {
            const {
                oldpassword,
                newpassword
            } = this.formGroup.get('pin').value;
            if (oldpassword === newpassword) {
                return swal({html:`<h4>New password cannot be the same as old password</h4>`})
            }
            this.store.dispatch(
                new UpdatePassword(this.formGroup.value.pin)
            )
        }
    }



    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.userSubscripton.unsubscribe();
    }




}
