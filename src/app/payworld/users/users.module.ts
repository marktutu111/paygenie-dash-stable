import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { MdModule } from 'app/md/md.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/app.module';
import { MatNativeDateModule, MatSelectModule } from '@angular/material';
import { SidebarModule } from 'app/sidebar/sidebar.module';
import { NavbarModule } from 'app/shared/navbar/navbar.module';
import { FooterModule } from 'app/shared/footer/footer.module';
import { FixedpluginModule } from 'app/shared/fixedplugin/fixedplugin.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserLoansComponent } from './user-loans/user-loans.component';
import { UserLoanRequestComponent } from './user-loan-request/user-loan-request.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { BillPayComponent } from "./billpay/billpay.component";
import { ProgressBarModule } from 'app/components/progress-bar/progress-bar.module';



const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full'
            },
            {
                path: 'profile',
                component: UserProfileComponent
            },
            {
                path: 'loans',
                component: UserLoansComponent
            },
            {
                path: 'loanrequest',
                component: UserLoanRequestComponent
            },
            {
                path: 'billpay',
                component: BillPayComponent
            }
        ]
    }
]




@NgModule({
    declarations: [
        UsersComponent,
        UserProfileComponent,
        UserLoansComponent,
        UserLoanRequestComponent,
        BillPayComponent
    ],
    imports: [ 
        CommonModule,
        MdModule,
        FormsModule,
        MaterialModule,
        MatNativeDateModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        FixedpluginModule,
        MatSelectModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        RouterModule.forChild(routes),
        ProgressBarModule
    ],
    exports: [],
    providers: [],
})
export class UsersModule {

}