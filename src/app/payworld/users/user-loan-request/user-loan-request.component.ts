import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UserState } from 'app/states/users/users.state';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserLoanRequest } from 'app/states/users/users.actions';
import banks from 'app/models/banks';
import { AppState } from 'app/states/app/state';
import { Toggle } from 'app/components/progress-bar/progress-bar.actions';
import { LoanService } from 'app/services/loans/loans.service';

declare const swal: any;


@Component({
    selector: 'app-user-loan-request',
    templateUrl: './user-loan-request.component.html',
    styleUrls: ['./user-loan-request.component.scss']
})
export class UserLoanRequestComponent implements OnInit {

    @Select(UserState.loading) loading$: Observable<boolean>;
    formGroup:FormGroup;
    banks:any[]=banks;
    settings:any=null;
    borrowLimit:any=100;


    constructor(private store: Store,private service:LoanService) {};


    ngOnInit(): void {
        try {

            this.formGroup = new FormGroup(
                {
                    amount: new FormControl(1, Validators.required),
                    account_number: new FormControl(null,Validators.required),
                    account_issuer: new FormControl(null,Validators.required),
                    account_type: new FormControl('momo',Validators.required),
                    pin: new FormControl(null,Validators.required)
                }
            )
            this.getSettings()
                .catch(err=>null);
        } catch (err) {
            
        }
    };


    async getSettings(){
        try {
            const user=this.store.selectSnapshot(AppState.user);
            this.store.dispatch(new Toggle(true));
            const response=await this.service.loadSLoanSettings(user.getEmployer());
            if(!response.success){
                throw Error(response.message);
            }
            this.store.dispatch(
                new Toggle(false)
            );
            this.settings=response.data;
            return this.calculateLimit();
        } catch (err) {
            this.store.dispatch(
                new Toggle(false)
            )
        }
    }


    calculateLimit() {
        try {
            const {
                maxnet
            } = this.store.selectSnapshot(AppState.user);
            return this.borrowLimit=(this.settings.borrow_limit/100) * parseFloat(maxnet);
        } catch (err) {
            return 100;
        }
    }
    
    
    async send () {
        try {

            const user = this.store.selectSnapshot(AppState.user);
            await this.getSettings();
            if(!this.settings){
                return swal(
                    {
                        html:`<h4>Hello ${user.firstname}, you have to be onboarded by your Employer before you can borrow.</h4>`
                    }
                )
            }
            if (this.formGroup.valid) {
                const {amount,account_number}=this.formGroup.value;
                const {rate,penalty,duration,duration_type}=this.settings;
                if (amount % 1 === 0 && amount <= this.borrowLimit){
                    return swal({
                        title: 'Borrow Money',
                        html: `<h4 style="text-align:left;">You have requested to borrow an amount of Ghs${amount} to account number ${account_number}. <p>Interest Rate: ${rate}%</p> <p>Amount to be paid: ${amount + (rate/100 * amount)}</p> <p>Duration: ${duration}${duration_type}</p> <p>Penalty: ${penalty}%</p> \n\n Please confirm your transaction.</h4>`,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, continue!'
                    }).then((result) => {
                        if (result) {
                            const data = this.formGroup.value;
                            const id:string = typeof user.empId === 'string' ? user.empId:user.empId._id;
                            return this.store.dispatch(
                                new UserLoanRequest(
                                    {
                                        ...data,
                                        amount: data.amount.toString(),
                                        customer: user['_id'],
                                        employer: id
                                    }
                                )
                            ).subscribe(()=>this.formGroup.patchValue(
                                {
                                    amount:1,
                                    account_number:null,
                                    account_issuer:null,
                                    account_type:'momo',
                                    pin:null
                                }
                            ));
                        }
                    })
                }
            }
        } catch (err) {
        }
    }



}
