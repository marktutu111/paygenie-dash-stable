
export interface UserLoginInterfaceModel {
    email: String;
	pswd: String;
	channel: String;
	request_id: String;
	request_type: String;
}



export interface ForgortPasswordInterfaceModel {
    usercode: String;
	channel: String;
	request_id: String;
	request_type: String;
}



export interface ResetPasswordInterfaceModel {
    usercode: String;
	pswd: String;
	otp: String;
	channel: String;
	request_id: String;
	request_type: String;
}




export interface NewUserInterfaceModel {
    firstname: String;
	lastname: String;
	gender: String;
	mobile: String;
	email: String;
	pswd: String;
	channel: String;
	dob: String;
	request_id: String;
	request_type: String;
}