import { LoanService } from 'app/services/loans/loans.service';


export class EmployerModel {

    empcode;
    name;
    email;
    mobile;
    digital_address;
    address;
    location;
    penalty_accrued=0;
    blocked=false;
    active;
    status;
    _id;
    createdAt;
    updatedAt;
    total_customers=0;
    total_loan_count=0;
    total_loan_amount=0;
    total_loan_due=0;

    constructor(employer,private service:LoanService){
        this.empcode=employer.empcode;
        this.name=employer.name;
        this.email=employer.email;
        this.digital_address=employer.digital_address;
        this.address=employer.address;
        this.location=employer.location;
        this.penalty_accrued=employer.penalty_accrued;
        this.blocked=employer.blocked;
        this.active=employer.active;
        this.status=employer.status;
        this._id=employer._id;
        this.createdAt=employer.createdAt;
        this.updatedAt=employer.updatedAt;

        this.getSummary();
        
    };

    async getSummary(){
        try {
            const response=await this.service.getEmployerSummary(this._id);
            if(!response.success){
                throw Error(response.message);
            }
            const summary=response.data[0];
            if(summary){
                this.total_loan_amount=summary['amountBorrowedWithInterest'];
                this.total_loan_count=summary['count'];
                this.total_loan_due=summary['amountRemaining'];
            }
        } catch (err) {
        }
    }

    getData(){
        return {
            empcode:this.empcode,
            name:this.name,
            email:this.email,
            mobile:this.mobile,
            digital_address:this.digital_address,
            address:this.address,
            location:this.location,
            penalty_accrued:this.penalty_accrued,
            blocked:this.blocked,
            active:this.active,
            status:this.status,
            createdAt:this.createdAt,
            updatedAt:this.updatedAt
        }
    }


}