import { BASE_URL } from 'app/apis';

export interface GetLoansInterfaceModel {
    channel: String;
    request_id: Number;
    request_type: String;
}


export interface ApplyLoanInterfaceModel {
    applicantid: Number;
    settings: Number;
    paymentaccount: String;
    amt: Number;
    paymentchannel: String;
    channel: String;
    request_id: String;
    request_type: String;
}


export interface LoansInterfaceModel {
    customer: any;
    employer: any;
    amount: number;
    amount_paid: number;
    amount_remaining: number;
    rate: number;
    penalty: number;
    next_payment_date: number;
    duration: number;
    duration_type: string;
    paidout: boolean;
    status: string;
    defaulted: boolean;
    createdAt: string;
    updatedAt: string;
    penalty_amount: number;
    maxLoan:number;
    canBorrow:boolean;
    _id:string;
    count:number;
    can_borrow_amount:number;
}



export class LoanModel implements LoansInterfaceModel {

    rate;
    amount_paid;
    penalty;
    duration;
    amount;
    amount_remaining;
    createdAt;
    customer;
    defaulted;
    duration_type;
    employer;
    next_payment_date;
    paidout;
    status;
    updatedAt;
    penalty_amount;
    maxLoan;
    canBorrow;
    _id;
    count;
    can_borrow_amount;

    constructor (loans: LoansInterfaceModel) {
        this.customer = loans.customer;
        this.employer = loans.employer;
        this.defaulted = loans.defaulted;
        this.duration = loans.duration;
        this.next_payment_date=loans.next_payment_date;
        this.status=loans.status;
        this.updatedAt=loans.updatedAt;
        this.createdAt=loans.createdAt;
        this.duration_type=loans.duration_type;
        this.rate = loans.rate;
        this.penalty = loans.penalty;
        this.paidout=loans.paidout;
        this.amount_remaining=loans.amount_remaining;
        this.amount_paid=loans.amount_paid;
        this.amount =loans.amount;
        this.penalty_amount =loans.penalty_amount;
        this.maxLoan=loans.maxLoan;
        this.canBorrow=loans.canBorrow;
        this._id=loans._id;
        this.count=loans.count;
        this.can_borrow_amount=loans.can_borrow_amount;

    };

    getEmployer(key):string{
        try {
            return this.employer[key];
        } catch (err) {
            return this.employer;
        }
    }

    getName():string{
        try {
            const {firstname,lastname}=this.customer;
            return `${firstname} ${lastname}`;
        } catch (err) {
            return '';
        }
    }

    getDuration () {
        try {
            return `${this.duration} ${this.duration_type}`;
        } catch (err) {
            return '';
        }
    }

    getStatus () {
        try {
            return typeof this.status === 'string' ? this.status : 'failed';
        } catch (err) {
            return 'failed';
        }
    }

    enablePay () {
        if (this.paidout && this.status === 'pending') {
            return true;
        }
        return false;
    }

    getDefaulted=()=>this.defaulted ? 'Yes' : 'No';



}