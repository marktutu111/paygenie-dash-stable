import * as moment from "moment";


export interface CustomersInterfaceModel {
	_id:string;
	firstname: string;
	lastname: string;
	empId: string;
	empcode: string;
	gender: string;
	maxnet: number;
	mobile: string;
	email: string;
	password: string;
	dob: string;
	approved: boolean;
	phone_verified: boolean;
	email_verified: boolean;
	blocked: boolean;
	active: boolean;
	loans_count: number;
	total_loans_taken: number;
	loans_failed: number;
	loans_successful: number;
	total_loan_paid: number;
	current_loan: number;
	current_loan_id: string;
	createdAt: string;
	updatedAt: string;

}




export class CustomersModel implements CustomersInterfaceModel {

	_id:string;
	firstname: string;
	lastname: string;
	empId: string;
	empcode: string;
	gender: string;
	maxnet: number;
	mobile: string;
	email: string;
	password: string;
	dob: string;
	approved: boolean;
	phone_verified: boolean;
	email_verified: boolean;
	blocked: boolean;
	active: boolean;
	loans_count: number;
	total_loans_taken: number;
	loans_failed: number;
	loans_successful: number;
	total_loan_paid: number;
	current_loan: number;
	current_loan_id: string;
	createdAt: string;
	updatedAt: string;


	constructor (customer: CustomersInterfaceModel) {
		this.firstname=customer.firstname;
		this.lastname=customer.lastname;
		this.gender=customer.gender;
		this.loans_count=customer.loans_count;
		this.loans_failed=customer.loans_failed;
		this.loans_successful=customer.loans_successful;
		this.maxnet=customer.maxnet;
		this.mobile=customer.mobile;
		this.password=customer.password;
		this.phone_verified=customer.phone_verified;
		this.total_loan_paid=customer.total_loan_paid;
		this.total_loans_taken=customer.total_loans_taken;
		this.updatedAt=customer.updatedAt;
		this.createdAt=customer.createdAt;
		this.current_loan_id=customer.current_loan_id;
		this.empId=customer.empId;
		this.empcode=customer.empcode;
		this.mobile=customer.mobile;
		this._id=customer._id;
		this.email=customer.email;
		this.dob=moment(customer.dob).format('YYYY-MM-DD');
		this.approved=customer.approved;
		this.email_verified=customer.email_verified;
		this.blocked=customer.blocked;
		this.active=customer.active;
		this.current_loan=customer.current_loan;
	}


	getName () {
		try {
			return `${this.firstname} ${this.lastname}`;
		} catch (err) {
			return '';
			
		}
	}

	getDate(){
		return moment(this.dob).format('YYYY-MM-DD');
	}

	getApproved () {
		if (this.approved) {
			return 'APPROVED';
		} else return 'NOT APPROVED';
	}

	getEmployer=(key:string='_id')=>{
		try {
			if (Object.keys(this.empId).length > 0) {
				return this.empId[key];
			}
			return this.empId;
		} catch (err) {
			return '';
		}
	}

	getLoan=(key:string='_id')=>{
		try {
			if(Object.keys(this.current_loan_id).length > 0) {
				return this.current_loan_id[key];
			}
			return this.current_loan_id;
		} catch (err) {
			return '';
		}
	}

	getData() {
		return {
			firstname: this.firstname,
			lastname:this.lastname,
			gender:this.gender,
			mobile:this.mobile,
			email:this.email,
			dob:this.dob,
			approved:this.approved,
			maxnet:this.maxnet,
			blocked:this.blocked,
			_id:this._id
		}
	}


}