

export interface BillIntefaceModel {
    _id:string;
    customer:string;
    sid:string;
    name:string;
    serviceDetails:string;
    status:string;
    createdAt:string;
    updatedAt:string;
}


export class BillModel implements BillIntefaceModel {

    _id;
    createdAt;
    customer;
    name;
    serviceDetails;
    sid;
    status;
    updatedAt;

    constructor (bill:BillIntefaceModel){
        this._id=bill._id;
        this.createdAt=bill.createdAt;
        this.customer=bill.customer;
        this.name=bill.name;
        this.serviceDetails=bill.serviceDetails;
        this.sid=bill.sid;
        this.status=bill.status;
        this.updatedAt=bill.updatedAt;
    }

    getStatus(){
        return this.status || 'pending';
    }

    getAmount=()=>{
        try {
            if (Object.keys(this.serviceDetails).length >0){
                return this.serviceDetails.amount;
            }
            return '';
        } catch (err) {
            return '';
        }
    }

}