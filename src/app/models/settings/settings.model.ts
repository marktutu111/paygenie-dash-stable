
export class SettingsModel {

    _id:string;
    type:string;
    employer:any;
    penalty:string;
    rate:string;
    duration:string;
    duration_type:string;
    borrow_limit:number;

    constructor (settings){
        this.type=settings.type;
        this.employer=settings.employer;
        this.penalty=settings.penalty;
        this.rate=settings.rate;
        this.duration=settings.duration;
        this.duration_type=settings.duration_type;
        this._id=settings._id;
        this.borrow_limit=settings.borrow_limit;
    };

    getEmployer(key){
        try {
            return this.employer[key];
        } catch (err) {
            return null;
        }
    }

    getEmployerName(){
        try {
            if (typeof this.employer && this.employer._id){
                return this.employer.name;
            }
            return 'Not Available';
        } catch (err) {
            return 'Not Available';
        }
    }

    getEmployerId(){
        try {
            return this.employer._id;
        } catch (err) {
            return '';
        }
    }

}