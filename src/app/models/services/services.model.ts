

interface ServiceInterfaceModel {
    "type": string;
    "_id": string;
    "name": string;
    "sid_list": string;
    "sid_buy": string;
    "createdAt":string;
    "updatedAt": string;
}

export class ServiceModel implements ServiceInterfaceModel {

    _id;
    createdAt;
    name;
    sid_buy;
    sid_list;
    updatedAt;
    type;

    private content:any[]= [];
    
    constructor (service:ServiceInterfaceModel) {
        this._id=service._id;
        this.createdAt=service.createdAt;
        this.updatedAt=service.updatedAt;
        this.sid_buy=service.sid_buy;
        this.sid_list=service.sid_list;
        this.type=service.type;
        this.name=service.name;
    };

    getType=()=>{
        switch (this.type) {
            case 'GLOW-DATA':
                return 'a';
            case 'MTN-DATA':
                return 'a';
            case 'DSTV':
                return 'b';
            case 'ECG':
                return 'b';
            case 'GO-TV':
                return 'b';
            default:
                return '';
        }
    }

    geFormValues=(item,form)=>{
        if (!item) return;
        if (this.getType() === 'a'){
            switch (form) {
                case 'data_bundle_type':
                    return item['productID'];
                case 'amount':
                    return item.getData('price');
                default:
                    return '';
            }
        }
        if (this.getType() === 'b'){
            switch (form) {
                case 'customerNumber':
                    return item['number'];
                case 'is_monthly':
                    return 'true';
                case 'amount':
                    return item['balance']
                default:
                    return '';
            }
        }
    }

    getForms=():string[]=>{
        if (this.getType() === 'a') {
            return [
                'msisdn',
                'data_bundle_type',
                'amount'
            ]
        }
        if (this.getType() === 'b') {
            return [
                'customerNumber',
                'is_monthly',
                'amount'
            ]
        }
        return [];
    }


    getPlaceholder=(key:string='')=>{
        const data={
            'msisdn':'Enter your phonenumber',
            'data_bundle_type':'Bundle id',
            'amount':'Purchase amount',
            'customerNumber':'Customer number',
            'is_monthly':'monthly subscription'
        }
        return data[key];
    }


    getDisabled=(v)=>{
        const disabled=[
            'data_bundle_type',
            'amount',
            'is_monthly'
        ]
        return disabled.indexOf(v) > -1 ? true:false;
    }


}


export class ServiceListModel {

    productID:string;
    description:{
        "volume": string;
        "name": string;
        "validity":string;
        "price":string;
    };

    constructor (data){
        this.productID=data.productID;
        this.description=data.description;
    };

    getDescription=()=>{
        const {validity,volume,price,name}=this.description;
        return `Ghs${price} ~ ${volume} for ${validity}`;
    };

    getData=(key)=>this.description[key];


}


export class DstvModel {
    "number": string; 
    "mobilePhone": string;
    "balance": string; 
    "descriptionTag": string; 
    "name": string; 
    "label": string;
    "type": string;
    "category": string;
    "shortName": string;
    "homeTelephone": string;
    "status": string;
    "sid": string;

    constructor (data){
        this.balance=data.balance;
        this.category=data.category;
        this.descriptionTag=data.descriptionTag;
        this.homeTelephone=data.homeTelephone;
        this.label=data.label;
        this.mobilePhone=data.mobilePhone;
        this.name=data.name;
        this.number=data.number;
        this.shortName=data.shortName;
        this.sid=data.sid;
        this.status=data.status;
        this.type=data.type;
    };

    
}