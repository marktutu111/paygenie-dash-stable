import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/apis';

@Injectable()
export class CustomerService {

    constructor (private http: HttpClient) {};


    newEmployer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/api/employer_setup`,data)
                        .toPromise();
    }

    
    authenticatEmployer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/api/employer_authenticate`,data)
                        .toPromise();
    }


}