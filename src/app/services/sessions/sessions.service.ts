import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Logout } from 'app/states/app/actions';
import { AppState } from 'app/states/app/state';

declare const swal: any;


@Injectable()
export class SessionService {


        timeout:any;
        autoLogout:any;

        constructor (private store:Store) {
                this.startSession();
        };

        startSession(){
                this.setSession();
                window.onmousemove=()=>{
                        if (this.timeout)clearTimeout(this.timeout);
                        this.setSession();
                }
        }

        logout(){
                const message=`<h4>You were logged out to protect your, Kindly login to continue.</h4>`;
                this.store.dispatch(new Logout());
                swal.fire({html:message});
                return window.removeEventListener('mousemove', null);
        }

        setSession () {
                const user = this.store.selectSnapshot(AppState.user);
                if (user && user['_id']) {
                        this.timeout = setTimeout(() => {
                                clearTimeout(this.timeout);
                                window.removeEventListener('mousemove', null);
                                swal(
                                        {
                                                html: '<h4>To protect your account, you will be logged out of your session. Click OK to continue using BMT</h4>',
                                                showCancelButton:true,
                                                cancelButtonText:'No, Logout',
                                                cancelButtonColor:'brown',
                                                confirmButtonText:'Yes, Continue'
                                        }
                                ).then(({value})=>{
                                        if(value){
                                                clearTimeout(this.autoLogout);
                                                return this.startSession();
                                        }
                                        return this.logout();
                                }).catch(err=>null);
                                this.autoLogout=setTimeout(() => {
                                        this.logout();
                                }, 1000*30);
                        }, 1000*60);
                }

        }

}