import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/apis';

@Injectable()
export class EmployerService {


    constructor (private http: HttpClient) {};


    getEmployerLoansApplicants (id:string): Promise<any> {
        return this.http.get(`${BASE_URL}/employer/get/loans/${id}`)
                        .toPromise();
    }


    getCustomers (id): Promise<any> {
        return this.http.get(`${BASE_URL}/employer/get/customers/${id}`)
                        .toPromise();
    }

    getEmployer (id): Promise<any> {
        return this.http.get(`${BASE_URL}/employer/get/${id}`)
                        .toPromise();
    }

    getEmployers (): Promise<any> {
        return this.http.get(`${BASE_URL}/employer/get`)
                        .toPromise();
    }

    getInvoice (): Promise<any> {
        return this.http.get(`${BASE_URL}/invoice/get`)
                        .toPromise();
    }

    getInvoiceById (id:string): Promise<any> {
        return this.http.get(`${BASE_URL}/invoice/get/${id}`)
                        .toPromise();
    }

    addCustomer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/new`,data)
                        .toPromise();
    }

    payInvoice (data): Promise<any> {
        return this.http.post(`${BASE_URL}/invoice/pay`,data)
                        .toPromise();
    }

    approveAll(data:any):Promise<any>{
        return this.http.post(`${BASE_URL}/employer/approveall`,data)
                        .toPromise();
    }

    addEmployer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/employer/new`,data)
                        .toPromise();
    }


    deleteCustomer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/app_onboard`,data)
                        .toPromise();
    }


    updateEmployer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/employer/update`,data)
                        .toPromise();
    }

    updateCustomer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/update`,data)
                        .toPromise();
    }

    changePassword (data): Promise<any> {
        return this.http.put(`${BASE_URL}/employer/changepassword`,data)
                        .toPromise();
    }

    approveCustomer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/approve`,data)
                        .toPromise();
    }

    approveEmployer (data): Promise<any> {
        console.log(data);
        
        return this.http.post(`${BASE_URL}/employer/approve`,data)
                        .toPromise();
    }

    authenticateEmployer (data): Promise<any> {
        return this.http.post(`${BASE_URL}/employer/login`,data)
                        .toPromise();
    }

    adminLogin (data): Promise<any> {
        return this.http.post(`${BASE_URL}/admin/login`,data)
                        .toPromise();
    }


    uploadBulk (data): Promise<any> {
        return this.http.post(`${BASE_URL}/appbulk_onboard`, data)
                        .toPromise();
    }

    
    filterByDate (data): Promise<any> {
        return this.http.post(`${BASE_URL}/loans/filter/date`, data)
                        .toPromise();
    }


    filterByPaymentsDue (data): Promise<any> {
        return this.http.get(`${BASE_URL}/loans/get/due`)
                        .toPromise();
    }


    loanAggregate (data): Promise<any> {
        return this.http.post(`${BASE_URL}/employerloan_agregator`,data)
                        .toPromise();
    }

    

}