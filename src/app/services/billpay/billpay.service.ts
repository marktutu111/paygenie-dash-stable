import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/apis';

@Injectable()
export class BillPayService {

    constructor (private http: HttpClient) {};

    getServices (): Promise<any> {
        return this.http.get(`${BASE_URL}/bills/service/get`)
                        .toPromise();
    }

    getServiceList (data): Promise<any> {
        return this.http.post(`${BASE_URL}/bills/service/get/list`,data)
                        .toPromise();
    }

    payBill (data): Promise<any> {
        return this.http.post(`${BASE_URL}/bills/service/pay`,data)
                        .toPromise();
    }

    getBills (customer:string): Promise<any> {
        return this.http.get(`${BASE_URL}/bills/get/${customer}`)
                        .toPromise();
    }


}