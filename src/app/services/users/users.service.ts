import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/apis';
import { NewUserInterfaceModel, ForgortPasswordInterfaceModel, ResetPasswordInterfaceModel, UserLoginInterfaceModel } from 'app/models/users/users.model';

@Injectable()
export class UserService {

    constructor (private http: HttpClient) {};


    updateUser (data: NewUserInterfaceModel): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/update`,data)
                        .toPromise();
    }

    updatePassword (data: NewUserInterfaceModel): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/update-password`,data)
                        .toPromise();
    }

    
    forgortPassword (data: ForgortPasswordInterfaceModel): Promise<any> {
        return this.http.post(`${BASE_URL}/forgot_password`,data)
                        .toPromise();
    }

    sendOtp (data): Promise<any> {
        return this.http.post(`${BASE_URL}/otp/sendotp`,data)
                        .toPromise();
    }


    resetPassword (data: ResetPasswordInterfaceModel): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/reset-password`, data)
                 .toPromise();
    }

    blockCustomer (data:any): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/block`, data)
                 .toPromise();
    }


    login (data: UserLoginInterfaceModel): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/login`,data)
                        .toPromise();
    }

    getLogins (data:{phone:string}): Promise<any> {
        return this.http.post(`${BASE_URL}/customers/login/get`,data)
                        .toPromise();
    }



    getloans (id:string): Promise<any> {
        return this.http.get(`${BASE_URL}/loans/get/${id}`)
                        .toPromise();
    }

    getUsers (): Promise<any> {
        return this.http.get(`${BASE_URL}/customers/get`)
                        .toPromise();
    }

    getTransaction (id:string): Promise<any> {
        return this.http.get(`${BASE_URL}/transactions/get/${id}`)
                        .toPromise();
    }

    getProfile (id:string): Promise<any> {
        return this.http.get(`${BASE_URL}/customers/get/${id}`)
                        .toPromise();
    }

    getUserSummary (id:string): Promise<any> {
        return this.http.get(`${BASE_URL}/loans/customer/summary/${id}`)
                        .toPromise();
    }


    loanRequest (data): Promise<any> {
        return this.http.post(`${BASE_URL}/loans/new`, data)
                        .toPromise();
    }

    payLoan (data): Promise<any> {
        return this.http.post(`${BASE_URL}/loans/pay`, data)
                        .toPromise();
    } 


    changePin (data): Promise<any> {
        return  this.http.post(`${BASE_URL}/change_pin`, data)
                         .toPromise();
    }


    resendOtp (data): Promise<any> {
        return  this.http.post(`${BASE_URL}/forgot_password`, data)
                         .toPromise();
    }
    

}