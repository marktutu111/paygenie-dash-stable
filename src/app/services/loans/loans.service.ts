import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BASE_URL } from 'app/apis';
import { GetLoansInterfaceModel, ApplyLoanInterfaceModel } from 'app/models/loans/loans.model';

@Injectable()
export class LoanService {

    constructor (private http: HttpClient) {};

    applyLoan (data: ApplyLoanInterfaceModel): Promise<any> {
        return this.http.post(`${BASE_URL}/app_getloan`,data)
                        .toPromise();
    }

    getLoans (): Promise<any> {
        return this.http.get(`${BASE_URL}/loans/get`)
                        .toPromise();
    }

    getLoanTransactions (id:string): Promise<any> {
        return this.http.get(`${BASE_URL}/transactions/get/${id}`)
                        .toPromise();
    }

    getEmployerSummary (id:string):Promise<any> {
        return this.http.get(`${BASE_URL}/loans/employer/summary/${id}`)
                        .toPromise();
    }

    getSettings ():Promise<any> {
        return this.http.get(`${BASE_URL}/loansettings/get`)
                        .toPromise();
    }

    setSettings (data): Promise<any> {
        return this.http.post(`${BASE_URL}/loansettings/new`,data)
                        .toPromise();
    }

    loadSLoanSettings(id:string):Promise<any>{
        return this.http.get(`${BASE_URL}/loansettings/get/${id}`)
                .toPromise();
    }

}