import { Injectable, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngxs/store';
import { AppState } from 'app/states/app/state';

import * as moment from "moment";
declare const swal: any;


class CustomerModel {
    firstname;
    lastname;
    gender;
    mobile;
    email;
    maxnet;
    dob;
    empId;
    empcode;
    status:'failed'|'saved'|'pending'='pending';
    loading:boolean=false;
    approved:boolean=true;

    constructor (data) {
        this.firstname=data.firstname;
        this.lastname=data.lastname;
        this.gender=data.gender;
        this.mobile=data.mobile;
        this.email=data.email;
        this.maxnet=data.maxnet;
        this.dob=data.dob;
        this.empId=data.empId;
        this.empcode=data.empcode;
    }

    setStatus (status) {
        this.status=status;
    }

    setLoading (loading){
        this.loading=loading;
    }

    getData () {
        return {
            firstname:this.firstname,
            lastname:this.lastname,
            gender:this.gender,
            mobile:this.mobile,
            email:this.email,
            maxnet:this.maxnet,
            dob:this.dob,
            empId:this.empId,
            empcode:this.empcode,
            approved:this.approved,
            control:'employer'
        }
    }

}



@Injectable({
    providedIn: 'root'
})
export class UploaderService {

    @Output() onCSV$:EventEmitter<any>=new EventEmitter();
    private customers:any[]=[];

    constructor(private store:Store){};

    onCSVChange (e) {
        try {

            const { name }=e.target.files[0];
            const file_type=name.split('.')[1];
            this.customers=[];

            const validateEmail=(email)=>(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/).test(email);
    
            if (file_type === 'csv') {       
                const fileReader = new FileReader();
                fileReader.onloadend = () => {
                    const buffer = fileReader.result;
                    const csv = buffer.toString().split(/\r\n|\n/);
                    csv.forEach(account => {
                        const _account = account.split(',');
                        let mobile: string;
                        if (_account[3][0].indexOf('0') > -1) {
                            mobile = _account[3];
                        } else {
                            mobile = `0${_account[3]}`;
                        }
                        const customer = {
                            firstname: _account[0],
                            lastname: _account[1],
                            gender: _account[2],
                            mobile: mobile,
                            email: _account[4],
                            dob: _account[5],
                            maxnet: _account[6],
                            empId:null,
                            empcode:null
                        };
                        if(!moment(customer.dob).isValid){
                            return swal(
                                `<h4>The CVS has errors, Please check customer with name <b>${customer.firstname} ${customer.lastname}</b>. Date of Birth might not be correct.</h4>`
                            )
                        }
                        if(!validateEmail(customer.email) || customer.mobile.length !== 10){
                            return swal(
                                `<h4>The CVS has errors, Please check customer with name <b>${customer.firstname} ${customer.lastname}</b>. Email or Phone Number might not be correct.</h4>`
                            )
                        };

                        this.customers.push(new CustomerModel(customer));

                    });

                    this.onCSV$.emit(this.customers);

                }
                return fileReader.readAsBinaryString(e.target.files[0]);
            } throw Error('Unsupported file type');
        } catch (err) {
            swal({html: `<h4>${err.message}</h4>`});
        }

    }

}