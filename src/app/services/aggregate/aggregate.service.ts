import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_URL } from 'app/apis';

@Injectable()
export class AggregateService {

    constructor (private http: HttpClient) {};

    getSummary (): Promise<any> {
        return this.http.get(`${BASE_URL}/aggregate/get`)
                        .toPromise();
    }

    getAdminSummary (): Promise<any> {
        return this.http.get(`${BASE_URL}/admin/summary/get`)
                        .toPromise();
    }


}